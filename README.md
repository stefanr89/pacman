# README #

Klon igrice Pacman, napisan u Java programskom jeziku. Program sluzi za testiranje JWBTT alata za whitebox testiranje.

### What is this repository for? ###

* Klon igrice Pacman (implementira veci deo funkcionalnosti originalne igre). Kretanje protivnika (duhova) se obavlja tako sto svaki duh ima svoj thread. Ovo je namerno uradjeno da bi se demonstrirao ispravan rad JWBTT alata pri radu sa multithreaded sistemima.
* Version 1.0.0

### How do I get set up? ###

* Projekat se pokrece kao i svaki drugi Java projekat.
* Pri izradi nisu koriscene third party biblioteke.
* Za testiranje su korisceni junit 4 i hamcrest.

### Contribution guidelines ###

* Ovaj projekat je deo master rada na Elektrotehnickom Fakultetu u Beogradu. Sve izmene i testiranje vrsi Stefan Rankovic. Otvaranje projekta za javnost bice eventualno moguce posle uspesne odbrane master rada, ukoliko mentor to odobri i ukoliko je to u skladu sa pravilnikom ETF.

### Who do I talk to? ###

* Stefan Rankovic (Repo Owner)