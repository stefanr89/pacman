package data;

import static org.junit.Assert.*;

import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

import org.junit.Test;

import sound.PacmanSound;

public class WorldTest {
	@Test
	public void testMovement() {
		World world = new World(new PacmanSound());
		assertTrue("Unable to move the valid move", world.canMove(1, 1, Direction.RIGHT));
		assertTrue("Unable to move the valid move", world.canMove(1, 1, Direction.DOWN));
		assertFalse("Able to take invalid move", world.canMove(1, 1, Direction.UP));
		assertFalse("Able to take invalid move", world.canMove(1, 1, Direction.LEFT));
		assertTrue("Unable to move through the tunnel from left", world.canMove(0, 14, Direction.LEFT));
		assertTrue("Unable to move through the tunnel from right", world.canMove(27, 14, Direction.RIGHT));
	}

	@Test
	public void testIntersectionDetection() {
		World world = new World(new PacmanSound());
		assertFalse("Not an intersectio but detected as one", world.isIntersection(1, 1));
		assertTrue("Intersection not detected", world.isIntersection(6, 1));
	}
	
	@Test
	public void testEatCell() {
		World world = new World(new PacmanSound());
		world.eatCell(1, 1);
		assertEquals("Dot in the cell was not eaten", world.getCell(1, 1), ' ');
		world.eatCell(0, 14);
		assertEquals("Empty cell was changed after eating", world.getCell(0, 14), ' ');
	}
}
