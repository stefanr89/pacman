package data;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class GhostModesTest {

	@Test
	public void testHuntMode() {
		Mode mode = new ModeHunt();
		List<Direction> possibleDirections = new LinkedList<Direction>();
		possibleDirections.add(Direction.UP);
		possibleDirections.add(Direction.DOWN);
		possibleDirections.add(Direction.LEFT);
		possibleDirections.add(Direction.RIGHT);
		assertEquals("Ghost should turn left", mode.bestDirection(1, 1, 0, 1, possibleDirections), Direction.LEFT);
	}
	
	@Test
	public void testFreightenMode() {
		Mode mode = new ModeFreighten();
		List<Direction> possibleDirections = new LinkedList<Direction>();
		possibleDirections.add(Direction.UP);
		possibleDirections.add(Direction.DOWN);
		possibleDirections.add(Direction.LEFT);
		possibleDirections.add(Direction.RIGHT);
		assertEquals("Ghost should turn right", mode.bestDirection(1, 1, 0, 1, possibleDirections), Direction.RIGHT);
	}

}
