import graphics.PacmanListener;
import graphics.WorldView;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import sound.PacmanSound;
import data.World;

public class Main {

    public static final String TITLE = "Pac Man";

    public static final int TICK_TIME = 50;

    public static final int PREP_TIME = 4600;

    public static void main(String[] args) {
        JWBTTWatcher$000000007.instance.passedStatement(0);
        Frame app = new Frame(TITLE);
        JWBTTWatcher$000000007.instance.passedStatement(1);
        app.setResizable(false);
        JWBTTWatcher$000000007.instance.passedStatement(2);
        app.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
        JWBTTWatcher$000000007.instance.passedStatement(3);
        PacmanSound sounds = new PacmanSound();
        JWBTTWatcher$000000007.instance.passedStatement(4);
        World world = new World(sounds);
        JWBTTWatcher$000000007.instance.passedStatement(5);
        WorldView board = new WorldView(world);
        JWBTTWatcher$000000007.instance.passedStatement(6);
        app.add(board);
        JWBTTWatcher$000000007.instance.passedStatement(7);
        app.pack();
        JWBTTWatcher$000000007.instance.passedStatement(8);
        app.setVisible(true);
        JWBTTWatcher$000000007.instance.passedStatement(9);
        PacmanListener pacListener = new PacmanListener(world);
        JWBTTWatcher$000000007.instance.passedStatement(10);
        app.addKeyListener(pacListener);
        JWBTTWatcher$000000007.instance.passedStatement(11);
        while (World.MORE_GAME) {
            JWBTTWatcher$000000007.instance.passedStatement(12);
            boolean preparation = true;
            JWBTTWatcher$000000007.instance.passedStatement(13);
            board.reload();
            JWBTTWatcher$000000007.instance.passedStatement(14);
            board.repaint();
            JWBTTWatcher$000000007.instance.passedStatement(15);
            while (!world.gameOver()) {
                JWBTTWatcher$000000007.instance.passedStatement(16);
                try {
                    JWBTTWatcher$000000007.instance.passedStatement(17);
                    if (preparation) {
                        JWBTTWatcher$000000007.instance.branchConditionTrue(0);
                        JWBTTWatcher$000000007.instance.passedStatement(18);
                        sounds.playIntro();
                        JWBTTWatcher$000000007.instance.passedStatement(19);
                        Thread.sleep(PREP_TIME);
                        JWBTTWatcher$000000007.instance.passedStatement(20);
                        preparation = false;
                        JWBTTWatcher$000000007.instance.passedStatement(21);
                        sounds.playSiren();
                    } else {
                        JWBTTWatcher$000000007.instance.branchConditionFalse(0);
                    }
                    JWBTTWatcher$000000007.instance.passedStatement(22);
                    world.tick();
                    JWBTTWatcher$000000007.instance.passedStatement(23);
                    board.repaint();
                    JWBTTWatcher$000000007.instance.passedStatement(24);
                    Thread.sleep(TICK_TIME);
                } catch (InterruptedException e) {
                    JWBTTWatcher$000000007.instance.passedStatement(25);
                    e.printStackTrace();
                }
                JWBTTWatcher$000000007.instance.executedLoop(1);
            }
            JWBTTWatcher$000000007.instance.exitedLoop(1);
            JWBTTWatcher$000000007.instance.passedStatement(26);
            sounds.stopAllSounds();
            JWBTTWatcher$000000007.instance.passedStatement(27);
            while (world.gameOver()) {
                JWBTTWatcher$000000007.instance.passedStatement(28);
                world.tick();
                JWBTTWatcher$000000007.instance.passedStatement(29);
                board.repaint();
                JWBTTWatcher$000000007.instance.passedStatement(30);
                try {
                    JWBTTWatcher$000000007.instance.passedStatement(31);
                    Thread.sleep(TICK_TIME);
                } catch (InterruptedException e) {
                    JWBTTWatcher$000000007.instance.passedStatement(32);
                    e.printStackTrace();
                }
                JWBTTWatcher$000000007.instance.executedLoop(2);
            }
            JWBTTWatcher$000000007.instance.exitedLoop(2);
            JWBTTWatcher$000000007.instance.executedLoop(0);
        }
        JWBTTWatcher$000000007.instance.exitedLoop(0);
    }
}

class JWBTTWatcher$000000007 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000007 instance = new JWBTTWatcher$000000007();

	 private JWBTTWatcher$000000007() {
		 super("Main.java", 33, 3, 1);
	 }

}