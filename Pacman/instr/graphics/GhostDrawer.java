package graphics;

import java.awt.Color;
import java.awt.Graphics;
import data.Ghost;
import data.World;

public class GhostDrawer {

    private Ghost ghost;

    private int scale;

    public GhostDrawer(Ghost ghost, int scale) {
        JWBTTWatcher$000000012.instance.passedStatement(0);
        this.ghost = ghost;
        JWBTTWatcher$000000012.instance.passedStatement(1);
        this.scale = scale;
    }

    public void draw(Graphics g) {
        JWBTTWatcher$000000012.instance.passedStatement(2);
        if (ghost.inFreightenMode()) {
            JWBTTWatcher$000000012.instance.branchConditionTrue(0);
            JWBTTWatcher$000000012.instance.passedStatement(3);
            if (ghost.getWorld().getPowerupTimer() > World.POWERUP_DANGER) {
                JWBTTWatcher$000000012.instance.branchConditionTrue(1);
                JWBTTWatcher$000000012.instance.passedStatement(4);
                g.setColor(new Color(60, 60, 205));
            } else {
                JWBTTWatcher$000000012.instance.branchConditionFalse(1);
                JWBTTWatcher$000000012.instance.passedStatement(5);
                if (ghost.getWorld().getPowerupTimer() % 20 > 10) {
                    JWBTTWatcher$000000012.instance.branchConditionTrue(2);
                    JWBTTWatcher$000000012.instance.passedStatement(6);
                    g.setColor(new Color(255, 255, 255));
                } else {
                    JWBTTWatcher$000000012.instance.branchConditionFalse(2);
                    JWBTTWatcher$000000012.instance.passedStatement(7);
                    g.setColor(new Color(60, 60, 205));
                }
            }
        } else {
            JWBTTWatcher$000000012.instance.branchConditionFalse(0);
            JWBTTWatcher$000000012.instance.passedStatement(8);
            g.setColor(ghost.getColor());
        }
        JWBTTWatcher$000000012.instance.passedStatement(9);
        g.fillOval((int) (ghost.getX() * scale), (int) (ghost.getY() * scale), scale, scale);
        JWBTTWatcher$000000012.instance.passedStatement(10);
        g.fillRect((int) (ghost.getX() * scale), (int) (ghost.getY() * scale) + scale / 2, scale, scale / 2);
        JWBTTWatcher$000000012.instance.passedStatement(11);
        g.setColor(Color.WHITE);
        JWBTTWatcher$000000012.instance.passedStatement(12);
        g.fillOval((int) (ghost.getX() * scale) + scale / 4, (int) (ghost.getY() * scale) + scale / 2, scale / 5, scale / 5);
        JWBTTWatcher$000000012.instance.passedStatement(13);
        g.fillOval((int) (ghost.getX() * scale) + scale / 2, (int) (ghost.getY() * scale) + scale / 2, scale / 5, scale / 5);
    }
}

class JWBTTWatcher$000000012 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000012 instance = new JWBTTWatcher$000000012();

	 private JWBTTWatcher$000000012() {
		 super("graphics/GhostDrawer.java", 14, 0, 3);
	 }

}