package graphics;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import data.Direction;
import data.World;

public class PacmanListener implements KeyListener {

    private World world;

    public PacmanListener(World world) {
        JWBTTWatcher$000000010.instance.passedStatement(0);
        this.world = world;
    }

    @Override
    public void keyPressed(KeyEvent key) {
        JWBTTWatcher$000000010.instance.passedStatement(1);
        int code = key.getKeyCode();
        JWBTTWatcher$000000010.instance.passedStatement(2);
        switch(code) {
            case KeyEvent.VK_UP:
                JWBTTWatcher$000000010.instance.passedStatement(3);
                world.getPacman().setDirection(Direction.UP);
                JWBTTWatcher$000000010.instance.passedStatement(4);
                break;
            case KeyEvent.VK_DOWN:
                JWBTTWatcher$000000010.instance.passedStatement(5);
                world.getPacman().setDirection(Direction.DOWN);
                JWBTTWatcher$000000010.instance.passedStatement(6);
                break;
            case KeyEvent.VK_LEFT:
                JWBTTWatcher$000000010.instance.passedStatement(7);
                world.getPacman().setDirection(Direction.LEFT);
                JWBTTWatcher$000000010.instance.passedStatement(8);
                break;
            case KeyEvent.VK_RIGHT:
                JWBTTWatcher$000000010.instance.passedStatement(9);
                world.getPacman().setDirection(Direction.RIGHT);
                JWBTTWatcher$000000010.instance.passedStatement(10);
                break;
            case KeyEvent.VK_SPACE:
                JWBTTWatcher$000000010.instance.passedStatement(11);
                if (world.gameOver()) {
                    JWBTTWatcher$000000010.instance.branchConditionTrue(0);
                    JWBTTWatcher$000000010.instance.passedStatement(12);
                    world.newGame();
                } else {
                    JWBTTWatcher$000000010.instance.branchConditionFalse(0);
                    JWBTTWatcher$000000010.instance.passedStatement(13);
                    if (world.levelComplete()) {
                        JWBTTWatcher$000000010.instance.branchConditionTrue(1);
                        JWBTTWatcher$000000010.instance.passedStatement(14);
                        world.nextLevel();
                    } else {
                        JWBTTWatcher$000000010.instance.branchConditionFalse(1);
                    }
                }
                JWBTTWatcher$000000010.instance.passedStatement(15);
                break;
            case KeyEvent.VK_ESCAPE:
                JWBTTWatcher$000000010.instance.passedStatement(16);
                World.MORE_GAME = false;
                JWBTTWatcher$000000010.instance.passedStatement(17);
                System.exit(0);
                JWBTTWatcher$000000010.instance.passedStatement(18);
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
    }

    @Override
    public void keyTyped(KeyEvent arg0) {
    }
}

class JWBTTWatcher$000000010 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000010 instance = new JWBTTWatcher$000000010();

	 private JWBTTWatcher$000000010() {
		 super("graphics/PacmanListener.java", 19, 0, 2);
	 }

}