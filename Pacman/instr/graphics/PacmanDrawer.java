package graphics;

import java.awt.Color;
import java.awt.Graphics;
import data.Direction;
import data.Pacman;

public class PacmanDrawer {

    private Pacman pacman;

    private int scale;

    private int drawingIteration;

    public PacmanDrawer(Pacman pacman, int scale) {
        JWBTTWatcher$000000011.instance.passedStatement(0);
        this.pacman = pacman;
        JWBTTWatcher$000000011.instance.passedStatement(1);
        this.scale = scale;
        JWBTTWatcher$000000011.instance.passedStatement(2);
        drawingIteration = 0;
    }

    public void draw(Graphics g) {
        JWBTTWatcher$000000011.instance.passedStatement(3);
        g.setColor(Color.YELLOW);
        JWBTTWatcher$000000011.instance.passedStatement(4);
        int startAngle = 0;
        JWBTTWatcher$000000011.instance.passedStatement(5);
        int arcAngle = 300;
        JWBTTWatcher$000000011.instance.passedStatement(6);
        if (!pacman.isDead()) {
            JWBTTWatcher$000000011.instance.branchConditionTrue(0);
            JWBTTWatcher$000000011.instance.passedStatement(7);
            startAngle -= drawingIteration * 5;
            JWBTTWatcher$000000011.instance.passedStatement(8);
            arcAngle += drawingIteration * 10;
        } else {
            JWBTTWatcher$000000011.instance.branchConditionFalse(0);
        }
        JWBTTWatcher$000000011.instance.passedStatement(9);
        if (pacman.getDirection() == Direction.LEFT) {
            JWBTTWatcher$000000011.instance.branchConditionTrue(1);
            JWBTTWatcher$000000011.instance.passedStatement(10);
            startAngle += 210;
        } else {
            JWBTTWatcher$000000011.instance.branchConditionFalse(1);
            JWBTTWatcher$000000011.instance.passedStatement(11);
            if (pacman.getDirection() == Direction.RIGHT) {
                JWBTTWatcher$000000011.instance.branchConditionTrue(2);
                JWBTTWatcher$000000011.instance.passedStatement(12);
                startAngle += 30;
            } else {
                JWBTTWatcher$000000011.instance.branchConditionFalse(2);
                JWBTTWatcher$000000011.instance.passedStatement(13);
                if (pacman.getDirection() == Direction.UP) {
                    JWBTTWatcher$000000011.instance.branchConditionTrue(3);
                    JWBTTWatcher$000000011.instance.passedStatement(14);
                    startAngle += 120;
                } else {
                    JWBTTWatcher$000000011.instance.branchConditionFalse(3);
                    JWBTTWatcher$000000011.instance.passedStatement(15);
                    if (pacman.getDirection() == Direction.DOWN) {
                        JWBTTWatcher$000000011.instance.branchConditionTrue(4);
                        JWBTTWatcher$000000011.instance.passedStatement(16);
                        startAngle += 300;
                    } else {
                        JWBTTWatcher$000000011.instance.branchConditionFalse(4);
                        JWBTTWatcher$000000011.instance.passedStatement(17);
                        System.err.println("Unknown direction.");
                    }
                }
            }
        }
        JWBTTWatcher$000000011.instance.passedStatement(18);
        g.fillArc((int) (pacman.getX() * scale), (int) (pacman.getY() * scale), scale, scale, startAngle, arcAngle);
        JWBTTWatcher$000000011.instance.passedStatement(19);
        drawingIteration = (drawingIteration + 1) % 7;
    }
}

class JWBTTWatcher$000000011 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000011 instance = new JWBTTWatcher$000000011();

	 private JWBTTWatcher$000000011() {
		 super("graphics/PacmanDrawer.java", 20, 0, 5);
	 }

}