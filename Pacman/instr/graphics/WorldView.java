package graphics;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import data.Ghost;
import data.World;

public class WorldView extends Canvas {

    private static final long serialVersionUID = 1L;

    private World world;

    private PacmanDrawer pd;

    private GhostDrawer[] gd;

    private final int scale = 16;

    private final int foodDiameter = scale / 4;

    private final int powerupDiameter = scale / 2;

    public WorldView(World world) {
        JWBTTWatcher$000000009.instance.passedStatement(0);
        this.world = world;
        JWBTTWatcher$000000009.instance.passedStatement(1);
        reload();
        JWBTTWatcher$000000009.instance.passedStatement(2);
        addKeyListener(new PacmanListener(world));
        JWBTTWatcher$000000009.instance.passedStatement(3);
        setSize(world.getWidth() * scale, world.getHeight() * scale);
    }

    public void reload() {
        JWBTTWatcher$000000009.instance.passedStatement(4);
        this.pd = new PacmanDrawer(world.getPacman(), scale);
        JWBTTWatcher$000000009.instance.passedStatement(5);
        Ghost[] ghosts = world.getGhosts();
        JWBTTWatcher$000000009.instance.passedStatement(6);
        this.gd = new GhostDrawer[ghosts.length];
        JWBTTWatcher$000000009.instance.passedStatement(7);
        for (int i = 0; i < ghosts.length; i++) {
            JWBTTWatcher$000000009.instance.passedStatement(8);
            gd[i] = new GhostDrawer(ghosts[i], scale);
            JWBTTWatcher$000000009.instance.executedLoop(0);
        }
        JWBTTWatcher$000000009.instance.exitedLoop(0);
    }

    public void paint(Graphics g) {
        JWBTTWatcher$000000009.instance.passedStatement(9);
        g.setColor(Color.BLACK);
        JWBTTWatcher$000000009.instance.passedStatement(10);
        g.fillRect(0, 0, getWidth(), getHeight());
        JWBTTWatcher$000000009.instance.passedStatement(11);
        for (int x = 0; x < world.getWidth(); x++) {
            JWBTTWatcher$000000009.instance.passedStatement(12);
            for (int y = 0; y < world.getHeight(); y++) {
                JWBTTWatcher$000000009.instance.passedStatement(13);
                switch(world.getCell(x, y)) {
                    case '.':
                        JWBTTWatcher$000000009.instance.passedStatement(14);
                        g.setColor(Color.ORANGE);
                        JWBTTWatcher$000000009.instance.passedStatement(15);
                        g.fillOval(x * scale + (scale - foodDiameter) / 2, y * scale + (scale - foodDiameter) / 2, foodDiameter, foodDiameter);
                        JWBTTWatcher$000000009.instance.passedStatement(16);
                        break;
                    case 'o':
                        JWBTTWatcher$000000009.instance.passedStatement(17);
                        g.setColor(Color.ORANGE);
                        JWBTTWatcher$000000009.instance.passedStatement(18);
                        g.fillOval(x * scale + (scale - powerupDiameter) / 2, y * scale + (scale - powerupDiameter) / 2, powerupDiameter, powerupDiameter);
                        JWBTTWatcher$000000009.instance.passedStatement(19);
                        break;
                    case '#':
                        JWBTTWatcher$000000009.instance.passedStatement(20);
                        g.setColor(Color.BLUE);
                        JWBTTWatcher$000000009.instance.passedStatement(21);
                        g.fillRect(x * scale, y * scale, scale, scale);
                        JWBTTWatcher$000000009.instance.passedStatement(22);
                        break;
                    case '-':
                        JWBTTWatcher$000000009.instance.passedStatement(23);
                        g.setColor(Color.PINK);
                        JWBTTWatcher$000000009.instance.passedStatement(24);
                        g.fillRect(x * scale, y * scale, scale, scale);
                        JWBTTWatcher$000000009.instance.passedStatement(25);
                        break;
                    case ' ':
                        JWBTTWatcher$000000009.instance.passedStatement(26);
                        break;
                    default:
                        JWBTTWatcher$000000009.instance.passedStatement(27);
                        System.err.println("Unknown symbol: " + world.getCell(x, y));
                }
                JWBTTWatcher$000000009.instance.executedLoop(2);
            }
            JWBTTWatcher$000000009.instance.exitedLoop(2);
            JWBTTWatcher$000000009.instance.executedLoop(1);
        }
        JWBTTWatcher$000000009.instance.exitedLoop(1);
        JWBTTWatcher$000000009.instance.passedStatement(28);
        pd.draw(g);
        JWBTTWatcher$000000009.instance.passedStatement(29);
        for (int i = 0; i < gd.length; i++) {
            JWBTTWatcher$000000009.instance.passedStatement(30);
            gd[i].draw(g);
            JWBTTWatcher$000000009.instance.executedLoop(3);
        }
        JWBTTWatcher$000000009.instance.exitedLoop(3);
        JWBTTWatcher$000000009.instance.passedStatement(31);
        if (world.gameOver() && world.getTime() % 80 > 40) {
            JWBTTWatcher$000000009.instance.branchConditionTrue(0);
            JWBTTWatcher$000000009.instance.passedStatement(32);
            g.setColor(Color.WHITE);
            JWBTTWatcher$000000009.instance.passedStatement(33);
            g.setFont(new Font("TimesRoman", Font.BOLD, 48));
            JWBTTWatcher$000000009.instance.passedStatement(34);
            g.drawString("GAME OVER", 4 * scale, 14 * scale);
        } else {
            JWBTTWatcher$000000009.instance.branchConditionFalse(0);
        }
    }

    public void update(Graphics g) {
        JWBTTWatcher$000000009.instance.passedStatement(35);
        Image in = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        JWBTTWatcher$000000009.instance.passedStatement(36);
        paint(in.getGraphics());
        JWBTTWatcher$000000009.instance.passedStatement(37);
        g.drawImage(in, 0, 0, null);
    }
}

class JWBTTWatcher$000000009 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000009 instance = new JWBTTWatcher$000000009();

	 private JWBTTWatcher$000000009() {
		 super("graphics/WorldView.java", 38, 4, 1);
	 }

}