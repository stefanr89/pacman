package sound;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class PacmanSound {

    private final String pacmanIntroSongFile = "intro.wav";

    private final String pacmanEatingFile = "dot.wav";

    private final String pacmanPowerupFile = "energizer.wav";

    private final String pacmanSirenFile = "siren.wav";

    private final String pacmanDeathFile = "death.wav";

    private final String pacmanGhostFile = "ghost.wav";

    private boolean pacmanEating;

    private Clip intro;

    private Clip wakawaka;

    private Clip powerup;

    private Clip siren;

    private Clip death;

    private Clip ghost;

    public PacmanSound() {
        JWBTTWatcher$000000008.instance.passedStatement(0);
        try {
            JWBTTWatcher$000000008.instance.passedStatement(1);
            intro = AudioSystem.getClip();
            JWBTTWatcher$000000008.instance.passedStatement(2);
            ;
            JWBTTWatcher$000000008.instance.passedStatement(3);
            intro.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanIntroSongFile)));
            JWBTTWatcher$000000008.instance.passedStatement(4);
            wakawaka = AudioSystem.getClip();
            JWBTTWatcher$000000008.instance.passedStatement(5);
            wakawaka.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanEatingFile)));
            JWBTTWatcher$000000008.instance.passedStatement(6);
            powerup = AudioSystem.getClip();
            JWBTTWatcher$000000008.instance.passedStatement(7);
            powerup.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanPowerupFile)));
            JWBTTWatcher$000000008.instance.passedStatement(8);
            siren = AudioSystem.getClip();
            JWBTTWatcher$000000008.instance.passedStatement(9);
            siren.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanSirenFile)));
            JWBTTWatcher$000000008.instance.passedStatement(10);
            death = AudioSystem.getClip();
            JWBTTWatcher$000000008.instance.passedStatement(11);
            death.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanDeathFile)));
            JWBTTWatcher$000000008.instance.passedStatement(12);
            ghost = AudioSystem.getClip();
            JWBTTWatcher$000000008.instance.passedStatement(13);
            ghost.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanGhostFile)));
            JWBTTWatcher$000000008.instance.passedStatement(14);
            pacmanEating = false;
        } catch (Exception e) {
            JWBTTWatcher$000000008.instance.passedStatement(15);
            e.printStackTrace();
        }
    }

    public void playIntro() {
        JWBTTWatcher$000000008.instance.passedStatement(16);
        intro.start();
    }

    public void stopIntro() {
        JWBTTWatcher$000000008.instance.passedStatement(17);
        intro.stop();
        JWBTTWatcher$000000008.instance.passedStatement(18);
        intro.setFramePosition(0);
    }

    public int durationIntro() {
        JWBTTWatcher$000000008.instance.passedStatement(19);
        return (int) intro.getMicrosecondLength();
    }

    public void playPowerup() {
        JWBTTWatcher$000000008.instance.passedStatement(20);
        powerup.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stopPowerup() {
        JWBTTWatcher$000000008.instance.passedStatement(21);
        powerup.stop();
    }

    public void playGhost() {
        JWBTTWatcher$000000008.instance.passedStatement(22);
        ghost.start();
    }

    public void stopGhost() {
        JWBTTWatcher$000000008.instance.passedStatement(23);
        ghost.stop();
        JWBTTWatcher$000000008.instance.passedStatement(24);
        ghost.setFramePosition(0);
    }

    public void playWakaWaka() {
        JWBTTWatcher$000000008.instance.passedStatement(25);
        if (!pacmanEating) {
            JWBTTWatcher$000000008.instance.branchConditionTrue(0);
            JWBTTWatcher$000000008.instance.passedStatement(26);
            pacmanEating = true;
            JWBTTWatcher$000000008.instance.passedStatement(27);
            wakawaka.loop(Clip.LOOP_CONTINUOUSLY);
        } else {
            JWBTTWatcher$000000008.instance.branchConditionFalse(0);
        }
    }

    public int durationWakaWaka() {
        JWBTTWatcher$000000008.instance.passedStatement(28);
        return (int) (wakawaka.getMicrosecondLength() / 1000);
    }

    public void stopWakaWaka() {
        JWBTTWatcher$000000008.instance.passedStatement(29);
        if (pacmanEating) {
            JWBTTWatcher$000000008.instance.branchConditionTrue(1);
            JWBTTWatcher$000000008.instance.passedStatement(30);
            pacmanEating = false;
            JWBTTWatcher$000000008.instance.passedStatement(31);
            wakawaka.stop();
            JWBTTWatcher$000000008.instance.passedStatement(32);
            wakawaka.setFramePosition(0);
        } else {
            JWBTTWatcher$000000008.instance.branchConditionFalse(1);
        }
    }

    public void playSiren() {
        JWBTTWatcher$000000008.instance.passedStatement(33);
        FloatControl gainControl = (FloatControl) siren.getControl(FloatControl.Type.MASTER_GAIN);
        JWBTTWatcher$000000008.instance.passedStatement(34);
        gainControl.setValue(-10.0f);
        JWBTTWatcher$000000008.instance.passedStatement(35);
        siren.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stopSiren() {
        JWBTTWatcher$000000008.instance.passedStatement(36);
        siren.stop();
    }

    public void playDeath() {
        JWBTTWatcher$000000008.instance.passedStatement(37);
        death.setMicrosecondPosition(1000000);
        JWBTTWatcher$000000008.instance.passedStatement(38);
        death.start();
    }

    public void stopAllSounds() {
        JWBTTWatcher$000000008.instance.passedStatement(39);
        stopIntro();
        JWBTTWatcher$000000008.instance.passedStatement(40);
        stopWakaWaka();
        JWBTTWatcher$000000008.instance.passedStatement(41);
        stopSiren();
        JWBTTWatcher$000000008.instance.passedStatement(42);
        stopPowerup();
        JWBTTWatcher$000000008.instance.passedStatement(43);
        stopGhost();
    }
}

class JWBTTWatcher$000000008 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000008 instance = new JWBTTWatcher$000000008();

	 private JWBTTWatcher$000000008() {
		 super("sound/PacmanSound.java", 44, 0, 2);
	 }

}