package data;

import java.awt.Color;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Ghost implements Runnable {

    private float ghx;

    private float ghy;

    private Color color;

    private Direction currentDirection;

    private World world;

    private int lastX;

    private int lastY;

    private int iteration;

    private boolean started;

    private Mode currentMode;

    private ModeHunt hm;

    private ModeFreighten fm;

    private int eatenTimeout;

    private Semaphore mySem;

    public Ghost(World world, float ghx, float ghy, Color color) {
        JWBTTWatcher$000000000.instance.passedStatement(0);
        hm = new ModeHunt();
        JWBTTWatcher$000000000.instance.passedStatement(1);
        fm = new ModeFreighten();
        JWBTTWatcher$000000000.instance.passedStatement(2);
        eatenTimeout = 0;
        JWBTTWatcher$000000000.instance.passedStatement(3);
        currentMode = hm;
        JWBTTWatcher$000000000.instance.passedStatement(4);
        this.world = world;
        JWBTTWatcher$000000000.instance.passedStatement(5);
        this.ghx = ghx;
        JWBTTWatcher$000000000.instance.passedStatement(6);
        this.ghy = ghy;
        JWBTTWatcher$000000000.instance.passedStatement(7);
        this.color = color;
        JWBTTWatcher$000000000.instance.passedStatement(8);
        currentDirection = Direction.UP;
        JWBTTWatcher$000000000.instance.passedStatement(9);
        mySem = new Semaphore(0);
        JWBTTWatcher$000000000.instance.passedStatement(10);
        lastX = 0;
        JWBTTWatcher$000000000.instance.passedStatement(11);
        lastY = 0;
        JWBTTWatcher$000000000.instance.passedStatement(12);
        iteration = 0;
        JWBTTWatcher$000000000.instance.passedStatement(13);
        started = false;
    }

    public synchronized float getX() {
        JWBTTWatcher$000000000.instance.passedStatement(14);
        return ghx;
    }

    public synchronized float getY() {
        JWBTTWatcher$000000000.instance.passedStatement(15);
        return ghy;
    }

    public Color getColor() {
        JWBTTWatcher$000000000.instance.passedStatement(16);
        return color;
    }

    public World getWorld() {
        JWBTTWatcher$000000000.instance.passedStatement(17);
        return world;
    }

    public synchronized void setHuntMode() {
        JWBTTWatcher$000000000.instance.passedStatement(18);
        currentMode = hm;
    }

    public synchronized void setFrightenMode() {
        JWBTTWatcher$000000000.instance.passedStatement(19);
        if (!world.inTheGhostHouse(Math.round(ghx), Math.round(ghy))) {
            JWBTTWatcher$000000000.instance.branchConditionTrue(0);
            JWBTTWatcher$000000000.instance.passedStatement(20);
            currentMode = fm;
        } else {
            JWBTTWatcher$000000000.instance.branchConditionFalse(0);
        }
    }

    public synchronized boolean inHuntMode() {
        JWBTTWatcher$000000000.instance.passedStatement(21);
        return currentMode == hm;
    }

    public synchronized boolean inFreightenMode() {
        JWBTTWatcher$000000000.instance.passedStatement(22);
        return currentMode == fm;
    }

    @Override
    public void run() {
        JWBTTWatcher$000000000.instance.passedStatement(23);
        started = true;
        JWBTTWatcher$000000000.instance.passedStatement(24);
        while (!world.gameOver()) {
            JWBTTWatcher$000000000.instance.passedStatement(25);
            if (eatenTimeout-- > 0) {
                JWBTTWatcher$000000000.instance.branchConditionTrue(1);
                JWBTTWatcher$000000000.instance.passedStatement(26);
                block();
                JWBTTWatcher$000000000.instance.passedStatement(27);
                continue;
            } else {
                JWBTTWatcher$000000000.instance.branchConditionFalse(1);
            }
            JWBTTWatcher$000000000.instance.passedStatement(28);
            int cellX;
            JWBTTWatcher$000000000.instance.passedStatement(29);
            int cellY;
            JWBTTWatcher$000000000.instance.passedStatement(30);
            int pacX;
            JWBTTWatcher$000000000.instance.passedStatement(31);
            int pacY;
            JWBTTWatcher$000000000.instance.passedStatement(32);
            float delta;
            JWBTTWatcher$000000000.instance.passedStatement(33);
            Direction currentDir;
            JWBTTWatcher$000000000.instance.passedStatement(34);
            synchronized (this) {
                JWBTTWatcher$000000000.instance.passedStatement(35);
                cellX = Math.round(ghx);
                JWBTTWatcher$000000000.instance.passedStatement(36);
                cellY = Math.round(ghy);
                JWBTTWatcher$000000000.instance.passedStatement(37);
                currentDir = currentDirection;
                JWBTTWatcher$000000000.instance.passedStatement(38);
                pacX = Math.round(world.getPacman().getX());
                JWBTTWatcher$000000000.instance.passedStatement(39);
                pacY = Math.round(world.getPacman().getY());
                JWBTTWatcher$000000000.instance.passedStatement(40);
                delta = currentMode.getDelta();
                JWBTTWatcher$000000000.instance.passedStatement(41);
                if (cellX == pacX && cellY == pacY) {
                    JWBTTWatcher$000000000.instance.branchConditionTrue(2);
                    JWBTTWatcher$000000000.instance.passedStatement(42);
                    if (inFreightenMode()) {
                        JWBTTWatcher$000000000.instance.branchConditionTrue(3);
                        JWBTTWatcher$000000000.instance.passedStatement(43);
                        ghx = 13.5f;
                        JWBTTWatcher$000000000.instance.passedStatement(44);
                        ghy = 14.0f;
                        JWBTTWatcher$000000000.instance.passedStatement(45);
                        currentMode = hm;
                        JWBTTWatcher$000000000.instance.passedStatement(46);
                        eatenTimeout = World.GHOST_EATEN_TIMEOUT;
                        JWBTTWatcher$000000000.instance.passedStatement(47);
                        world.killGhost();
                    } else {
                        JWBTTWatcher$000000000.instance.branchConditionFalse(3);
                        JWBTTWatcher$000000000.instance.passedStatement(48);
                        world.killPacman();
                    }
                    JWBTTWatcher$000000000.instance.passedStatement(49);
                    continue;
                } else {
                    JWBTTWatcher$000000000.instance.branchConditionFalse(2);
                }
            }
            JWBTTWatcher$000000000.instance.passedStatement(50);
            float nextGhx = ghx;
            JWBTTWatcher$000000000.instance.passedStatement(51);
            float nextGhy = ghy;
            JWBTTWatcher$000000000.instance.passedStatement(52);
            if ((cellX != lastX || cellY != lastY) && Math.abs(Math.round(ghx) - ghx) < delta / 2 && Math.abs(Math.round(ghy) - ghy) < delta / 2) {
                JWBTTWatcher$000000000.instance.branchConditionTrue(4);
                JWBTTWatcher$000000000.instance.passedStatement(53);
                lastX = cellX;
                JWBTTWatcher$000000000.instance.passedStatement(54);
                lastY = cellY;
                JWBTTWatcher$000000000.instance.passedStatement(55);
                nextGhx = cellX;
                JWBTTWatcher$000000000.instance.passedStatement(56);
                nextGhy = cellY;
                JWBTTWatcher$000000000.instance.passedStatement(57);
                List<Direction> possibleDirections = world.possibleDirections(cellX, cellY);
                JWBTTWatcher$000000000.instance.passedStatement(58);
                synchronized (this) {
                    JWBTTWatcher$000000000.instance.passedStatement(59);
                    possibleDirections.remove(currentDir.getOppositeDirection());
                }
                JWBTTWatcher$000000000.instance.passedStatement(60);
                if (world.inTheGhostHouse(cellX, cellY)) {
                    JWBTTWatcher$000000000.instance.branchConditionTrue(5);
                    JWBTTWatcher$000000000.instance.passedStatement(61);
                    int targetX = 13;
                    JWBTTWatcher$000000000.instance.passedStatement(62);
                    int targetY = 11;
                    JWBTTWatcher$000000000.instance.passedStatement(63);
                    currentDir = currentMode.bestDirection(cellX, cellY, targetX, targetY, possibleDirections);
                } else {
                    JWBTTWatcher$000000000.instance.branchConditionFalse(5);
                    JWBTTWatcher$000000000.instance.passedStatement(64);
                    if (iteration > 0) {
                        JWBTTWatcher$000000000.instance.branchConditionTrue(6);
                        JWBTTWatcher$000000000.instance.passedStatement(65);
                        currentDir = currentMode.bestDirection(cellX, cellY, pacX, pacY, possibleDirections);
                    } else {
                        JWBTTWatcher$000000000.instance.branchConditionFalse(6);
                        JWBTTWatcher$000000000.instance.passedStatement(66);
                        currentDir = possibleDirections.get((int) (Math.random() * possibleDirections.size()));
                    }
                }
                JWBTTWatcher$000000000.instance.passedStatement(67);
                iteration = (iteration + 1) % 4;
            } else {
                JWBTTWatcher$000000000.instance.branchConditionFalse(4);
            }
            JWBTTWatcher$000000000.instance.passedStatement(68);
            if (currentDir == Direction.UP) {
                JWBTTWatcher$000000000.instance.branchConditionTrue(7);
                JWBTTWatcher$000000000.instance.passedStatement(69);
                nextGhy -= delta;
            } else {
                JWBTTWatcher$000000000.instance.branchConditionFalse(7);
                JWBTTWatcher$000000000.instance.passedStatement(70);
                if (currentDir == Direction.DOWN) {
                    JWBTTWatcher$000000000.instance.branchConditionTrue(8);
                    JWBTTWatcher$000000000.instance.passedStatement(71);
                    nextGhy += delta;
                } else {
                    JWBTTWatcher$000000000.instance.branchConditionFalse(8);
                    JWBTTWatcher$000000000.instance.passedStatement(72);
                    if (currentDir == Direction.LEFT) {
                        JWBTTWatcher$000000000.instance.branchConditionTrue(9);
                        JWBTTWatcher$000000000.instance.passedStatement(73);
                        nextGhx -= delta;
                        JWBTTWatcher$000000000.instance.passedStatement(74);
                        if (nextGhx < -0.5f && Math.abs(ghy - world.getTunnelRow()) < delta / 2) {
                            JWBTTWatcher$000000000.instance.branchConditionTrue(10);
                            JWBTTWatcher$000000000.instance.passedStatement(75);
                            currentDir = currentDir.getOppositeDirection();
                        } else {
                            JWBTTWatcher$000000000.instance.branchConditionFalse(10);
                        }
                    } else {
                        JWBTTWatcher$000000000.instance.branchConditionFalse(9);
                        JWBTTWatcher$000000000.instance.passedStatement(76);
                        if (currentDir == Direction.RIGHT) {
                            JWBTTWatcher$000000000.instance.branchConditionTrue(11);
                            JWBTTWatcher$000000000.instance.passedStatement(77);
                            nextGhx += delta;
                            JWBTTWatcher$000000000.instance.passedStatement(78);
                            if (nextGhx >= world.getWidth() - 0.5f && Math.abs(ghy - world.getTunnelRow()) < delta / 2) {
                                JWBTTWatcher$000000000.instance.branchConditionTrue(12);
                                JWBTTWatcher$000000000.instance.passedStatement(79);
                                currentDir = currentDir.getOppositeDirection();
                            } else {
                                JWBTTWatcher$000000000.instance.branchConditionFalse(12);
                            }
                        } else {
                            JWBTTWatcher$000000000.instance.branchConditionFalse(11);
                        }
                    }
                }
            }
            JWBTTWatcher$000000000.instance.passedStatement(80);
            synchronized (this) {
                JWBTTWatcher$000000000.instance.passedStatement(81);
                if (world.canMove(cellX, cellY, currentDir)) {
                    JWBTTWatcher$000000000.instance.branchConditionTrue(13);
                    JWBTTWatcher$000000000.instance.passedStatement(82);
                    ghx = nextGhx;
                    JWBTTWatcher$000000000.instance.passedStatement(83);
                    ghy = nextGhy;
                } else {
                    JWBTTWatcher$000000000.instance.branchConditionFalse(13);
                    JWBTTWatcher$000000000.instance.passedStatement(84);
                    ghx = Math.round(ghx);
                    JWBTTWatcher$000000000.instance.passedStatement(85);
                    ghy = Math.round(ghy);
                }
                JWBTTWatcher$000000000.instance.passedStatement(86);
                currentDirection = currentDir;
            }
            JWBTTWatcher$000000000.instance.passedStatement(87);
            block();
            JWBTTWatcher$000000000.instance.executedLoop(0);
        }
        JWBTTWatcher$000000000.instance.exitedLoop(0);
    }

    private void block() {
        JWBTTWatcher$000000000.instance.passedStatement(88);
        try {
            JWBTTWatcher$000000000.instance.passedStatement(89);
            mySem.acquire();
        } catch (InterruptedException e) {
            JWBTTWatcher$000000000.instance.passedStatement(90);
            e.printStackTrace();
        }
    }

    public void tick() {
        JWBTTWatcher$000000000.instance.passedStatement(91);
        if (started) {
            JWBTTWatcher$000000000.instance.branchConditionTrue(14);
            JWBTTWatcher$000000000.instance.passedStatement(92);
            mySem.release();
            JWBTTWatcher$000000000.instance.passedStatement(93);
            if (world.getPowerupTimer() == 0) {
                JWBTTWatcher$000000000.instance.branchConditionTrue(15);
                JWBTTWatcher$000000000.instance.passedStatement(94);
                setHuntMode();
            } else {
                JWBTTWatcher$000000000.instance.branchConditionFalse(15);
            }
        } else {
            JWBTTWatcher$000000000.instance.branchConditionFalse(14);
        }
    }
}

class JWBTTWatcher$000000000 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000000 instance = new JWBTTWatcher$000000000();

	 private JWBTTWatcher$000000000() {
		 super("data/Ghost.java", 95, 1, 16);
	 }

}