package data;

public class Pacman {

    private Direction currentDirection;

    private Direction nextDirection;

    private World world;

    private float pacx;

    private float pacy;

    private float delta;

    private boolean dead;

    private static final float DEFAULT_DELTA = 0.2f;

    public Pacman(World world, float pacx, float pacy, float delta) {
        JWBTTWatcher$000000005.instance.passedStatement(0);
        currentDirection = Direction.LEFT;
        JWBTTWatcher$000000005.instance.passedStatement(1);
        nextDirection = Direction.LEFT;
        JWBTTWatcher$000000005.instance.passedStatement(2);
        this.world = world;
        JWBTTWatcher$000000005.instance.passedStatement(3);
        this.pacx = pacx;
        JWBTTWatcher$000000005.instance.passedStatement(4);
        this.pacy = pacy;
        JWBTTWatcher$000000005.instance.passedStatement(5);
        this.delta = delta;
        JWBTTWatcher$000000005.instance.passedStatement(6);
        this.dead = false;
    }

    public Pacman(World world, float pacx, float pacy) {
        this(world, pacx, pacy, DEFAULT_DELTA);
    }

    public void setDirection(Direction newDir) {
        JWBTTWatcher$000000005.instance.passedStatement(7);
        nextDirection = newDir;
    }

    public void tick() {
        JWBTTWatcher$000000005.instance.passedStatement(8);
        if (currentDirection.getOppositeDirection() == nextDirection) {
            JWBTTWatcher$000000005.instance.branchConditionTrue(0);
            JWBTTWatcher$000000005.instance.passedStatement(9);
            currentDirection = nextDirection;
        } else {
            JWBTTWatcher$000000005.instance.branchConditionFalse(0);
            JWBTTWatcher$000000005.instance.passedStatement(10);
            if (Math.abs(Math.round(pacx) - pacx) < delta / 2 && Math.abs(Math.round(pacy) - pacy) < delta / 2 && world.canMove(Math.round(pacx), Math.round(pacy), nextDirection)) {
                JWBTTWatcher$000000005.instance.branchConditionTrue(1);
                JWBTTWatcher$000000005.instance.passedStatement(11);
                currentDirection = nextDirection;
            } else {
                JWBTTWatcher$000000005.instance.branchConditionFalse(1);
            }
        }
        JWBTTWatcher$000000005.instance.passedStatement(12);
        float nextPacx = pacx;
        JWBTTWatcher$000000005.instance.passedStatement(13);
        float nextPacy = pacy;
        JWBTTWatcher$000000005.instance.passedStatement(14);
        if (currentDirection == Direction.UP) {
            JWBTTWatcher$000000005.instance.branchConditionTrue(2);
            JWBTTWatcher$000000005.instance.passedStatement(15);
            nextPacy -= delta;
        } else {
            JWBTTWatcher$000000005.instance.branchConditionFalse(2);
            JWBTTWatcher$000000005.instance.passedStatement(16);
            if (currentDirection == Direction.DOWN) {
                JWBTTWatcher$000000005.instance.branchConditionTrue(3);
                JWBTTWatcher$000000005.instance.passedStatement(17);
                nextPacy += delta;
            } else {
                JWBTTWatcher$000000005.instance.branchConditionFalse(3);
                JWBTTWatcher$000000005.instance.passedStatement(18);
                if (currentDirection == Direction.LEFT) {
                    JWBTTWatcher$000000005.instance.branchConditionTrue(4);
                    JWBTTWatcher$000000005.instance.passedStatement(19);
                    nextPacx -= delta;
                    JWBTTWatcher$000000005.instance.passedStatement(20);
                    if (nextPacx < -0.5f && Math.abs(pacy - world.getTunnelRow()) < delta / 2) {
                        JWBTTWatcher$000000005.instance.branchConditionTrue(5);
                        JWBTTWatcher$000000005.instance.passedStatement(21);
                        nextPacx = world.getWidth() - 1;
                    } else {
                        JWBTTWatcher$000000005.instance.branchConditionFalse(5);
                    }
                } else {
                    JWBTTWatcher$000000005.instance.branchConditionFalse(4);
                    JWBTTWatcher$000000005.instance.passedStatement(22);
                    if (currentDirection == Direction.RIGHT) {
                        JWBTTWatcher$000000005.instance.branchConditionTrue(6);
                        JWBTTWatcher$000000005.instance.passedStatement(23);
                        nextPacx += delta;
                        JWBTTWatcher$000000005.instance.passedStatement(24);
                        if (nextPacx >= world.getWidth() - 0.5f && Math.abs(pacy - world.getTunnelRow()) < delta / 2) {
                            JWBTTWatcher$000000005.instance.branchConditionTrue(7);
                            JWBTTWatcher$000000005.instance.passedStatement(25);
                            nextPacx = 0;
                        } else {
                            JWBTTWatcher$000000005.instance.branchConditionFalse(7);
                        }
                    } else {
                        JWBTTWatcher$000000005.instance.branchConditionFalse(6);
                    }
                }
            }
        }
        JWBTTWatcher$000000005.instance.passedStatement(26);
        int cellX = Math.round(pacx);
        JWBTTWatcher$000000005.instance.passedStatement(27);
        int cellY = Math.round(pacy);
        JWBTTWatcher$000000005.instance.passedStatement(28);
        world.eatCell(cellX, cellY);
        JWBTTWatcher$000000005.instance.passedStatement(29);
        if (world.canMove(cellX, cellY, currentDirection)) {
            JWBTTWatcher$000000005.instance.branchConditionTrue(8);
            JWBTTWatcher$000000005.instance.passedStatement(30);
            pacx = nextPacx;
            JWBTTWatcher$000000005.instance.passedStatement(31);
            pacy = nextPacy;
        } else {
            JWBTTWatcher$000000005.instance.branchConditionFalse(8);
            JWBTTWatcher$000000005.instance.passedStatement(32);
            pacx = Math.round(pacx);
            JWBTTWatcher$000000005.instance.passedStatement(33);
            pacy = Math.round(pacy);
        }
    }

    public synchronized float getX() {
        JWBTTWatcher$000000005.instance.passedStatement(34);
        return pacx;
    }

    public synchronized float getY() {
        JWBTTWatcher$000000005.instance.passedStatement(35);
        return pacy;
    }

    public synchronized Direction getDirection() {
        JWBTTWatcher$000000005.instance.passedStatement(36);
        return currentDirection;
    }

    public synchronized World getWorld() {
        JWBTTWatcher$000000005.instance.passedStatement(37);
        return world;
    }

    public synchronized boolean isDead() {
        JWBTTWatcher$000000005.instance.passedStatement(38);
        return dead;
    }

    public synchronized void setDead() {
        JWBTTWatcher$000000005.instance.passedStatement(39);
        dead = true;
    }
}

class JWBTTWatcher$000000005 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000005 instance = new JWBTTWatcher$000000005();

	 private JWBTTWatcher$000000005() {
		 super("data/Pacman.java", 40, 0, 9);
	 }

}