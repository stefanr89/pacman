package data;

import java.util.List;

public class ModeFreighten implements Mode {

    private static final float DEFAULT_DELTA = 0.12f;

    private float delta;

    public ModeFreighten() {
        JWBTTWatcher$000000004.instance.passedStatement(0);
        delta = DEFAULT_DELTA;
    }

    @Override
    public Direction bestDirection(int cellX, int cellY, int targetX, int tagetY, List<Direction> possibleDirections) {
        JWBTTWatcher$000000004.instance.passedStatement(1);
        double bestDistance = Double.NEGATIVE_INFINITY;
        JWBTTWatcher$000000004.instance.passedStatement(2);
        Direction bestDirection = null;
        JWBTTWatcher$000000004.instance.passedStatement(3);
        for (Direction d : possibleDirections) {
            JWBTTWatcher$000000004.instance.passedStatement(4);
            int myX = cellX;
            JWBTTWatcher$000000004.instance.passedStatement(5);
            int myY = cellY;
            JWBTTWatcher$000000004.instance.passedStatement(6);
            if (d == Direction.UP) {
                JWBTTWatcher$000000004.instance.branchConditionTrue(0);
                JWBTTWatcher$000000004.instance.passedStatement(7);
                myY -= 1;
            } else {
                JWBTTWatcher$000000004.instance.branchConditionFalse(0);
                JWBTTWatcher$000000004.instance.passedStatement(8);
                if (d == Direction.DOWN) {
                    JWBTTWatcher$000000004.instance.branchConditionTrue(1);
                    JWBTTWatcher$000000004.instance.passedStatement(9);
                    myY += 1;
                } else {
                    JWBTTWatcher$000000004.instance.branchConditionFalse(1);
                    JWBTTWatcher$000000004.instance.passedStatement(10);
                    if (d == Direction.LEFT) {
                        JWBTTWatcher$000000004.instance.branchConditionTrue(2);
                        JWBTTWatcher$000000004.instance.passedStatement(11);
                        myX -= 1;
                    } else {
                        JWBTTWatcher$000000004.instance.branchConditionFalse(2);
                        JWBTTWatcher$000000004.instance.passedStatement(12);
                        if (d == Direction.RIGHT) {
                            JWBTTWatcher$000000004.instance.branchConditionTrue(3);
                            JWBTTWatcher$000000004.instance.passedStatement(13);
                            myX += 1;
                        } else {
                            JWBTTWatcher$000000004.instance.branchConditionFalse(3);
                        }
                    }
                }
            }
            JWBTTWatcher$000000004.instance.passedStatement(14);
            double distance = Math.sqrt(Math.pow(targetX - myX, 2) + Math.pow(tagetY - myY, 2));
            JWBTTWatcher$000000004.instance.passedStatement(15);
            if (distance > bestDistance) {
                JWBTTWatcher$000000004.instance.branchConditionTrue(4);
                JWBTTWatcher$000000004.instance.passedStatement(16);
                bestDistance = distance;
                JWBTTWatcher$000000004.instance.passedStatement(17);
                bestDirection = d;
            } else {
                JWBTTWatcher$000000004.instance.branchConditionFalse(4);
            }
            JWBTTWatcher$000000004.instance.executedLoop(0);
        }
        JWBTTWatcher$000000004.instance.exitedLoop(0);
        JWBTTWatcher$000000004.instance.passedStatement(18);
        return bestDirection;
    }

    @Override
    public float getDelta() {
        JWBTTWatcher$000000004.instance.passedStatement(19);
        return delta;
    }
}

class JWBTTWatcher$000000004 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000004 instance = new JWBTTWatcher$000000004();

	 private JWBTTWatcher$000000004() {
		 super("data/ModeFreighten.java", 20, 1, 5);
	 }

}