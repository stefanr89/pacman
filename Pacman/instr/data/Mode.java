package data;

import java.util.List;

public interface Mode {

    Direction bestDirection(int cellX, int cellY, int targetX, int tagetY, List<Direction> possibleDirections);

    float getDelta();
}

class JWBTTWatcher$000000003 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000003 instance = new JWBTTWatcher$000000003();

	 private JWBTTWatcher$000000003() {
		 super("data/Mode.java", 0, 0, 0);
	 }

}