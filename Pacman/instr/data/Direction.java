package data;

public enum Direction {

    UP, DOWN, LEFT, RIGHT;

    private Direction opposite;

    static {
        JWBTTWatcher$000000001.instance.passedStatement(0);
        UP.opposite = DOWN;
        JWBTTWatcher$000000001.instance.passedStatement(1);
        DOWN.opposite = UP;
        JWBTTWatcher$000000001.instance.passedStatement(2);
        LEFT.opposite = RIGHT;
        JWBTTWatcher$000000001.instance.passedStatement(3);
        RIGHT.opposite = LEFT;
    }

    public Direction getOppositeDirection() {
        JWBTTWatcher$000000001.instance.passedStatement(4);
        return opposite;
    }
}

class JWBTTWatcher$000000001 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000001 instance = new JWBTTWatcher$000000001();

	 private JWBTTWatcher$000000001() {
		 super("data/Direction.java", 5, 0, 0);
	 }

}