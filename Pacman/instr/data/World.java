package data;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import sound.PacmanSound;

public class World {

    private final char[][] map = { { '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#' }, { '#', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '#', '#', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '#' }, { '#', '.', '#', '#', '#', '#', '.', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '.', '#', '#', '#', '#', '.', '#' }, { '#', 'o', '#', '#', '#', '#', '.', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '.', '#', '#', '#', '#', 'o', '#' }, { '#', '.', '#', '#', '#', '#', '.', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '.', '#', '#', '#', '#', '.', '#' }, { '#', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '#' }, { '#', '.', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '.', '#' }, { '#', '.', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '.', '#' }, { '#', '.', '.', '.', '.', '.', '.', '#', '#', '.', '.', '.', '.', '#', '#', '.', '.', '.', '.', '#', '#', '.', '.', '.', '.', '.', '.', '#' }, { '#', '#', '#', '#', '#', '#', '.', '#', '#', '#', '#', '#', ' ', '#', '#', ' ', '#', '#', '#', '#', '#', '.', '#', '#', '#', '#', '#', '#' }, { ' ', ' ', ' ', ' ', ' ', '#', '.', '#', '#', '#', '#', '#', ' ', '#', '#', ' ', '#', '#', '#', '#', '#', '.', '#', ' ', ' ', ' ', ' ', ' ' }, { ' ', ' ', ' ', ' ', ' ', '#', '.', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', '.', '#', ' ', ' ', ' ', ' ', ' ' }, { ' ', ' ', ' ', ' ', ' ', '#', '.', '#', '#', ' ', '#', '#', '#', '-', '-', '#', '#', '#', ' ', '#', '#', '.', '#', ' ', ' ', ' ', ' ', ' ' }, { '#', '#', '#', '#', '#', '#', '.', '#', '#', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', '#', '#', '.', '#', '#', '#', '#', '#', '#' }, { ' ', ' ', ' ', ' ', ' ', ' ', '.', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '.', ' ', ' ', ' ', ' ', ' ', ' ' }, { '#', '#', '#', '#', '#', '#', '.', '#', '#', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', '#', '#', '.', '#', '#', '#', '#', '#', '#' }, { ' ', ' ', ' ', ' ', ' ', '#', '.', '#', '#', ' ', '#', '#', '#', '#', '#', '#', '#', '#', ' ', '#', '#', '.', '#', ' ', ' ', ' ', ' ', ' ' }, { ' ', ' ', ' ', ' ', ' ', '#', '.', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', '.', '#', ' ', ' ', ' ', ' ', ' ' }, { ' ', ' ', ' ', ' ', ' ', '#', '.', '#', '#', ' ', '#', '#', '#', '#', '#', '#', '#', '#', ' ', '#', '#', '.', '#', ' ', ' ', ' ', ' ', ' ' }, { '#', '#', '#', '#', '#', '#', '.', '#', '#', ' ', '#', '#', '#', '#', '#', '#', '#', '#', ' ', '#', '#', '.', '#', '#', '#', '#', '#', '#' }, { '#', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '#', '#', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '#' }, { '#', '.', '#', '#', '#', '#', '.', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '.', '#', '#', '#', '#', '.', '#' }, { '#', '.', '#', '#', '#', '#', '.', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '.', '#', '#', '#', '#', '.', '#' }, { '#', 'o', '.', '.', '#', '#', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '#', '#', '.', '.', 'o', '#' }, { '#', '#', '#', '.', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '.', '#', '#', '#' }, { '#', '#', '#', '.', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '.', '#', '#', '#' }, { '#', '.', '.', '.', '.', '.', '.', '#', '#', '.', '.', '.', '.', '#', '#', '.', '.', '.', '.', '#', '#', '.', '.', '.', '.', '.', '.', '#' }, { '#', '.', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '.', '#' }, { '#', '.', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '.', '#', '#', '.', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '.', '#' }, { '#', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '#' }, { '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#' } };

    private char[][] walls;

    public static final int DOT_BONUS = 10;

    public static final int POWERUP_BONUS = 50;

    public static final int GHOST_KILL = 200;

    public static final int GHOST_BONUS_KILL = 400;

    public static final int POWERUP_DURATION = 250;

    public static final int POWERUP_DANGER = 100;

    public static final int GHOST_EATEN_TIMEOUT = 200;

    public static boolean MORE_GAME = true;

    private final int tunnelRow = 14;

    private int time;

    private int score;

    private boolean gameEnd;

    private Pacman pacman;

    private Ghost[] ghosts;

    private PacmanSound sounds;

    private int playFor;

    private int powerupTimer;

    private int dotsRemaining;

    public World(PacmanSound sounds) {
        JWBTTWatcher$000000006.instance.passedStatement(0);
        this.sounds = sounds;
        JWBTTWatcher$000000006.instance.passedStatement(1);
        restart();
        JWBTTWatcher$000000006.instance.passedStatement(2);
        score = 0;
        JWBTTWatcher$000000006.instance.passedStatement(3);
        playFor = 0;
        JWBTTWatcher$000000006.instance.passedStatement(4);
        time = 0;
        JWBTTWatcher$000000006.instance.passedStatement(5);
        powerupTimer = 0;
        JWBTTWatcher$000000006.instance.passedStatement(6);
        gameEnd = false;
    }

    public void restartTime() {
        JWBTTWatcher$000000006.instance.passedStatement(7);
        time = 0;
        JWBTTWatcher$000000006.instance.passedStatement(8);
        score = 0;
    }

    public void restart() {
        JWBTTWatcher$000000006.instance.passedStatement(9);
        walls = new char[map.length][map[0].length];
        JWBTTWatcher$000000006.instance.passedStatement(10);
        for (int i = 0; i < walls.length; i++) {
            JWBTTWatcher$000000006.instance.passedStatement(11);
            walls[i] = Arrays.copyOf(map[i], map[i].length);
            JWBTTWatcher$000000006.instance.executedLoop(0);
        }
        JWBTTWatcher$000000006.instance.exitedLoop(0);
        JWBTTWatcher$000000006.instance.passedStatement(12);
        dotsRemaining = 0;
        JWBTTWatcher$000000006.instance.passedStatement(13);
        for (int i = 0; i < getWidth(); i++) {
            JWBTTWatcher$000000006.instance.passedStatement(14);
            for (int j = 0; j < getHeight(); j++) {
                JWBTTWatcher$000000006.instance.passedStatement(15);
                if (isEatable(i, j)) {
                    JWBTTWatcher$000000006.instance.branchConditionTrue(0);
                    JWBTTWatcher$000000006.instance.passedStatement(16);
                    dotsRemaining++;
                } else {
                    JWBTTWatcher$000000006.instance.branchConditionFalse(0);
                }
                JWBTTWatcher$000000006.instance.executedLoop(2);
            }
            JWBTTWatcher$000000006.instance.exitedLoop(2);
            JWBTTWatcher$000000006.instance.executedLoop(1);
        }
        JWBTTWatcher$000000006.instance.exitedLoop(1);
        JWBTTWatcher$000000006.instance.passedStatement(17);
        pacman = new Pacman(this, 14, 23);
        JWBTTWatcher$000000006.instance.passedStatement(18);
        ghosts = new Ghost[4];
        JWBTTWatcher$000000006.instance.passedStatement(19);
        ghosts[0] = new Ghost(this, 13.5f, 11.0f, Color.RED);
        JWBTTWatcher$000000006.instance.passedStatement(20);
        ghosts[1] = new Ghost(this, 13.5f, 14.0f, Color.PINK);
        JWBTTWatcher$000000006.instance.passedStatement(21);
        ghosts[2] = new Ghost(this, 11.0f, 14.0f, Color.CYAN);
        JWBTTWatcher$000000006.instance.passedStatement(22);
        ghosts[3] = new Ghost(this, 16.0f, 14.0f, Color.ORANGE);
    }

    public Pacman getPacman() {
        JWBTTWatcher$000000006.instance.passedStatement(23);
        return pacman;
    }

    public Ghost[] getGhosts() {
        JWBTTWatcher$000000006.instance.passedStatement(24);
        return ghosts;
    }

    public int getWidth() {
        JWBTTWatcher$000000006.instance.passedStatement(25);
        return map[0].length;
    }

    public int getHeight() {
        JWBTTWatcher$000000006.instance.passedStatement(26);
        return map.length;
    }

    public char getCell(int x, int y) {
        JWBTTWatcher$000000006.instance.passedStatement(27);
        return walls[y][x];
    }

    public void eatCell(int x, int y) {
        JWBTTWatcher$000000006.instance.passedStatement(28);
        if (walls[y][x] == '.') {
            JWBTTWatcher$000000006.instance.branchConditionTrue(1);
            JWBTTWatcher$000000006.instance.passedStatement(29);
            sounds.playWakaWaka();
            JWBTTWatcher$000000006.instance.passedStatement(30);
            playFor = 10;
            JWBTTWatcher$000000006.instance.passedStatement(31);
            score += DOT_BONUS;
            JWBTTWatcher$000000006.instance.passedStatement(32);
            dotsRemaining--;
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(1);
            JWBTTWatcher$000000006.instance.passedStatement(33);
            if (walls[y][x] == 'o') {
                JWBTTWatcher$000000006.instance.branchConditionTrue(2);
                JWBTTWatcher$000000006.instance.passedStatement(34);
                sounds.playPowerup();
                JWBTTWatcher$000000006.instance.passedStatement(35);
                powerupTimer = POWERUP_DURATION;
                JWBTTWatcher$000000006.instance.passedStatement(36);
                for (Ghost g : ghosts) {
                    JWBTTWatcher$000000006.instance.passedStatement(37);
                    g.setFrightenMode();
                    JWBTTWatcher$000000006.instance.executedLoop(3);
                }
                JWBTTWatcher$000000006.instance.exitedLoop(3);
                JWBTTWatcher$000000006.instance.passedStatement(38);
                score += POWERUP_BONUS;
                JWBTTWatcher$000000006.instance.passedStatement(39);
                dotsRemaining--;
            } else {
                JWBTTWatcher$000000006.instance.branchConditionFalse(2);
            }
        }
        JWBTTWatcher$000000006.instance.passedStatement(40);
        if (levelComplete()) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(3);
            JWBTTWatcher$000000006.instance.passedStatement(41);
            gameEnd = true;
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(3);
        }
        JWBTTWatcher$000000006.instance.passedStatement(42);
        walls[y][x] = ' ';
    }

    public boolean isWall(int x, int y) {
        JWBTTWatcher$000000006.instance.passedStatement(43);
        if (x < 0 || x >= getWidth() || y < 0 || y >= getHeight()) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(4);
            JWBTTWatcher$000000006.instance.passedStatement(44);
            return false;
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(4);
        }
        JWBTTWatcher$000000006.instance.passedStatement(45);
        return walls[y][x] == '#';
    }

    public boolean isGhostWall(int x, int y) {
        JWBTTWatcher$000000006.instance.passedStatement(46);
        if (x < 0 || x >= getWidth() || y < 0 || y >= getHeight()) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(5);
            JWBTTWatcher$000000006.instance.passedStatement(47);
            return false;
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(5);
        }
        JWBTTWatcher$000000006.instance.passedStatement(48);
        return walls[y][x] == '-';
    }

    public boolean isEatable(int x, int y) {
        JWBTTWatcher$000000006.instance.passedStatement(49);
        if (x < 0 || x >= getWidth() || y < 0 || y >= getHeight()) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(6);
            JWBTTWatcher$000000006.instance.passedStatement(50);
            return false;
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(6);
        }
        JWBTTWatcher$000000006.instance.passedStatement(51);
        return walls[y][x] == '.' || walls[y][x] == 'o';
    }

    public boolean inTheGhostHouse(int x, int y) {
        JWBTTWatcher$000000006.instance.passedStatement(52);
        return x > 10 && x < 17 && y > 12 && y < 17;
    }

    public List<Direction> possibleDirections(int x, int y) {
        JWBTTWatcher$000000006.instance.passedStatement(53);
        List<Direction> dirs = new ArrayList<Direction>();
        JWBTTWatcher$000000006.instance.passedStatement(54);
        boolean inGhostHouse = inTheGhostHouse(x, y);
        JWBTTWatcher$000000006.instance.passedStatement(55);
        if (!isWall(x - 1, y) && (inGhostHouse || !isGhostWall(x - 1, y))) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(7);
            JWBTTWatcher$000000006.instance.passedStatement(56);
            dirs.add(Direction.LEFT);
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(7);
        }
        JWBTTWatcher$000000006.instance.passedStatement(57);
        if (!isWall(x + 1, y) && (inGhostHouse || !isGhostWall(x + 1, y))) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(8);
            JWBTTWatcher$000000006.instance.passedStatement(58);
            dirs.add(Direction.RIGHT);
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(8);
        }
        JWBTTWatcher$000000006.instance.passedStatement(59);
        if (!isWall(x, y - 1) && (inGhostHouse || !isGhostWall(x, y - 1))) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(9);
            JWBTTWatcher$000000006.instance.passedStatement(60);
            dirs.add(Direction.UP);
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(9);
        }
        JWBTTWatcher$000000006.instance.passedStatement(61);
        if (!isWall(x, y + 1) && (inGhostHouse || !isGhostWall(x, y + 1))) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(10);
            JWBTTWatcher$000000006.instance.passedStatement(62);
            dirs.add(Direction.DOWN);
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(10);
        }
        JWBTTWatcher$000000006.instance.passedStatement(63);
        return dirs;
    }

    public boolean isIntersection(int x, int y) {
        JWBTTWatcher$000000006.instance.passedStatement(64);
        return possibleDirections(x, y).size() >= 3;
    }

    public boolean canMove(int x, int y, Direction dir) {
        JWBTTWatcher$000000006.instance.passedStatement(65);
        int nextX = x;
        JWBTTWatcher$000000006.instance.passedStatement(66);
        int nextY = y;
        JWBTTWatcher$000000006.instance.passedStatement(67);
        if (dir == Direction.UP) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(11);
            JWBTTWatcher$000000006.instance.passedStatement(68);
            nextY -= 1;
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(11);
            JWBTTWatcher$000000006.instance.passedStatement(69);
            if (dir == Direction.DOWN) {
                JWBTTWatcher$000000006.instance.branchConditionTrue(12);
                JWBTTWatcher$000000006.instance.passedStatement(70);
                nextY += 1;
            } else {
                JWBTTWatcher$000000006.instance.branchConditionFalse(12);
                JWBTTWatcher$000000006.instance.passedStatement(71);
                if (dir == Direction.LEFT) {
                    JWBTTWatcher$000000006.instance.branchConditionTrue(13);
                    JWBTTWatcher$000000006.instance.passedStatement(72);
                    nextX -= 1;
                } else {
                    JWBTTWatcher$000000006.instance.branchConditionFalse(13);
                    JWBTTWatcher$000000006.instance.passedStatement(73);
                    if (dir == Direction.RIGHT) {
                        JWBTTWatcher$000000006.instance.branchConditionTrue(14);
                        JWBTTWatcher$000000006.instance.passedStatement(74);
                        nextX += 1;
                    } else {
                        JWBTTWatcher$000000006.instance.branchConditionFalse(14);
                    }
                }
            }
        }
        JWBTTWatcher$000000006.instance.passedStatement(75);
        if (nextX < 0 || nextX >= getWidth()) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(15);
            JWBTTWatcher$000000006.instance.passedStatement(76);
            if (nextY == tunnelRow) {
                JWBTTWatcher$000000006.instance.branchConditionTrue(16);
                JWBTTWatcher$000000006.instance.passedStatement(77);
                return true;
            } else {
                JWBTTWatcher$000000006.instance.branchConditionFalse(16);
                JWBTTWatcher$000000006.instance.passedStatement(78);
                return false;
            }
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(15);
        }
        JWBTTWatcher$000000006.instance.passedStatement(79);
        if (nextY < 0 || nextY >= getHeight()) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(17);
            JWBTTWatcher$000000006.instance.passedStatement(80);
            return false;
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(17);
        }
        JWBTTWatcher$000000006.instance.passedStatement(81);
        if (inTheGhostHouse(x, y)) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(18);
            JWBTTWatcher$000000006.instance.passedStatement(82);
            return !isWall(nextX, nextY);
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(18);
            JWBTTWatcher$000000006.instance.passedStatement(83);
            return !(isWall(nextX, nextY) || isGhostWall(nextX, nextY));
        }
    }

    public void tick() {
        JWBTTWatcher$000000006.instance.passedStatement(84);
        if (!gameOver()) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(19);
            JWBTTWatcher$000000006.instance.passedStatement(85);
            pacman.tick();
            JWBTTWatcher$000000006.instance.passedStatement(86);
            if (time == 0) {
                JWBTTWatcher$000000006.instance.branchConditionTrue(20);
                JWBTTWatcher$000000006.instance.passedStatement(87);
                new Thread(ghosts[0]).start();
                JWBTTWatcher$000000006.instance.passedStatement(88);
                new Thread(ghosts[1]).start();
            } else {
                JWBTTWatcher$000000006.instance.branchConditionFalse(20);
                JWBTTWatcher$000000006.instance.passedStatement(89);
                if (time == 60) {
                    JWBTTWatcher$000000006.instance.branchConditionTrue(21);
                    JWBTTWatcher$000000006.instance.passedStatement(90);
                    new Thread(ghosts[2]).start();
                } else {
                    JWBTTWatcher$000000006.instance.branchConditionFalse(21);
                    JWBTTWatcher$000000006.instance.passedStatement(91);
                    if (time == 200) {
                        JWBTTWatcher$000000006.instance.branchConditionTrue(22);
                        JWBTTWatcher$000000006.instance.passedStatement(92);
                        new Thread(ghosts[3]).start();
                    } else {
                        JWBTTWatcher$000000006.instance.branchConditionFalse(22);
                    }
                }
            }
            JWBTTWatcher$000000006.instance.passedStatement(93);
            for (int i = 0; i < ghosts.length; i++) {
                JWBTTWatcher$000000006.instance.passedStatement(94);
                ghosts[i].tick();
                JWBTTWatcher$000000006.instance.executedLoop(4);
            }
            JWBTTWatcher$000000006.instance.exitedLoop(4);
            JWBTTWatcher$000000006.instance.passedStatement(95);
            if (playFor > 0) {
                JWBTTWatcher$000000006.instance.branchConditionTrue(23);
                JWBTTWatcher$000000006.instance.passedStatement(96);
                playFor--;
            } else {
                JWBTTWatcher$000000006.instance.branchConditionFalse(23);
                JWBTTWatcher$000000006.instance.passedStatement(97);
                sounds.stopWakaWaka();
            }
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(19);
        }
        JWBTTWatcher$000000006.instance.passedStatement(98);
        synchronized (this) {
            JWBTTWatcher$000000006.instance.passedStatement(99);
            time++;
            JWBTTWatcher$000000006.instance.passedStatement(100);
            if (powerupTimer > 0) {
                JWBTTWatcher$000000006.instance.branchConditionTrue(24);
                JWBTTWatcher$000000006.instance.passedStatement(101);
                powerupTimer--;
                JWBTTWatcher$000000006.instance.passedStatement(102);
                if (powerupTimer == 0) {
                    JWBTTWatcher$000000006.instance.branchConditionTrue(25);
                    JWBTTWatcher$000000006.instance.passedStatement(103);
                    sounds.stopPowerup();
                } else {
                    JWBTTWatcher$000000006.instance.branchConditionFalse(25);
                }
            } else {
                JWBTTWatcher$000000006.instance.branchConditionFalse(24);
            }
        }
    }

    public synchronized int getTime() {
        JWBTTWatcher$000000006.instance.passedStatement(104);
        return time;
    }

    public synchronized int getScore() {
        JWBTTWatcher$000000006.instance.passedStatement(105);
        return score;
    }

    public synchronized void killPacman() {
        JWBTTWatcher$000000006.instance.passedStatement(106);
        sounds.playDeath();
        JWBTTWatcher$000000006.instance.passedStatement(107);
        pacman.setDead();
        JWBTTWatcher$000000006.instance.passedStatement(108);
        gameEnd = true;
    }

    public synchronized void killGhost() {
        JWBTTWatcher$000000006.instance.passedStatement(109);
        sounds.stopGhost();
        JWBTTWatcher$000000006.instance.passedStatement(110);
        sounds.playGhost();
        JWBTTWatcher$000000006.instance.passedStatement(111);
        if (powerupTimer > POWERUP_DANGER) {
            JWBTTWatcher$000000006.instance.branchConditionTrue(26);
            JWBTTWatcher$000000006.instance.passedStatement(112);
            score += GHOST_KILL;
        } else {
            JWBTTWatcher$000000006.instance.branchConditionFalse(26);
            JWBTTWatcher$000000006.instance.passedStatement(113);
            score += GHOST_BONUS_KILL;
        }
    }

    public boolean gameOver() {
        JWBTTWatcher$000000006.instance.passedStatement(114);
        return gameEnd;
    }

    public int getTunnelRow() {
        JWBTTWatcher$000000006.instance.passedStatement(115);
        return tunnelRow;
    }

    public int getPowerupTimer() {
        JWBTTWatcher$000000006.instance.passedStatement(116);
        return powerupTimer;
    }

    public void newGame() {
        JWBTTWatcher$000000006.instance.passedStatement(117);
        gameEnd = false;
        JWBTTWatcher$000000006.instance.passedStatement(118);
        restart();
        JWBTTWatcher$000000006.instance.passedStatement(119);
        restartTime();
    }

    public boolean levelComplete() {
        JWBTTWatcher$000000006.instance.passedStatement(120);
        return dotsRemaining == 0;
    }

    public void nextLevel() {
        JWBTTWatcher$000000006.instance.passedStatement(121);
        gameEnd = false;
        JWBTTWatcher$000000006.instance.passedStatement(122);
        restart();
    }
}

class JWBTTWatcher$000000006 extends jwbtt_support.JWBTTConcurrentWatcher {
	 public static final JWBTTWatcher$000000006 instance = new JWBTTWatcher$000000006();

	 private JWBTTWatcher$000000006() {
		 super("data/World.java", 123, 5, 27);
	 }

}