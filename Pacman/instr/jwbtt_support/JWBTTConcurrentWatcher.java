package jwbtt_support;

public class JWBTTConcurrentWatcher implements JWBTTWatcherBase {
	public final String filepath; // fajl koji ovaj watcher posmatra
	
	private long[] statement;
	private long[] loop; // posto je ovo samo counter, necemo serijalizovati
	
	private long[] loopExecutedZeroTimes;
	private long[] loopExecutedOnce;
	private long[] loopExecutedManyTimes;
	
	private long[] branchTaken;
	private long[] branchNotTaken;
	
	public JWBTTConcurrentWatcher(String filepath, int statementCnt, int loopCnt, int branchCnt) {
		this.filepath = filepath;
		
		statement = new long[statementCnt];
		
		loop = new long[loopCnt];
		loopExecutedZeroTimes = new long[loopCnt];
		loopExecutedOnce = new long[loopCnt];
		loopExecutedManyTimes = new long[loopCnt];
		
		branchTaken = new long[branchCnt];
		branchNotTaken = new long[branchCnt];
		
		JWBTTMonitor.register(this); // registrujemo se 
	}
	
	public synchronized void passedStatement(int i) {
		statement[i]++;
	}
	
	public synchronized void executedLoop(int i) {
		loop[i]++;
	}
	
	public synchronized void exitedLoop(int i) {
		if (loop[i] > 1) { // loop je izvrsen vise puta
			loopExecutedManyTimes[i]++;
		} else if (loop[i] == 1) { // loop je izvrsen tacno jedanput
			loopExecutedOnce[i]++;
		} else { // ako se uzme ovaj branch, znaci da je loop izvrsen nula puta
			loopExecutedZeroTimes[i]++;
		}
		loop[i] = 0; // reset
	}
	
	public synchronized void branchConditionTrue(int i) {
		branchTaken[i]++;
	}
	
	public synchronized void branchConditionFalse(int i) {
		branchNotTaken[i]++;
	}

	@Override
	public JWBTTReport createReport() {
		JWBTTReport report = new JWBTTReport();
		report.filepath = this.filepath;
		report.statement = this.statement;
		report.loopExecutedManyTimes = this.loopExecutedManyTimes;
		report.loopExecutedOnce = this.loopExecutedOnce;
		report.loopExecutedZeroTimes = this.loopExecutedZeroTimes;
		report.branchTaken = this.branchTaken;
		report.branchNotTaken = this.branchNotTaken;
		return report;
	}
}
