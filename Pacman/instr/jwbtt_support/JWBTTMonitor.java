package jwbtt_support;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public final class JWBTTMonitor {
	private static List<JWBTTWatcherBase> registeredWatchers = new LinkedList<JWBTTWatcherBase>();
	private static volatile boolean registrationOpen = true; // mogu se registrovati novi watcheri
	
	static {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				registrationOpen = false;
				
				ObjectOutputStream oos = null;
				try {
					oos = new ObjectOutputStream(new FileOutputStream("JWBTT_REPORT_" + System.currentTimeMillis() + ".ser"));
					try {
						oos.writeObject(registeredWatchers.size());
						for (JWBTTWatcherBase base: registeredWatchers) {
							oos.writeObject(base.createReport()); // upisemo celu listu odjednom
						}
					} catch (IOException e) {
						e.printStackTrace();
						System.exit(1);
					}
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
					System.exit(1);
				} catch (IOException e1) {
					e1.printStackTrace();
					System.exit(1);
				} finally {
					if (oos != null) {
						try {
							oos.close();
						} catch (IOException e) {
							e.printStackTrace();
							System.exit(1);
						}
					}
					
				}
			}
		});
	}
	
	public static void register(JWBTTWatcherBase watcher) { // ovde se watcheri registruju
		if (watcher == null) { // provera
			return;
		}
		if (registrationOpen) {
			registeredWatchers.add(watcher);
		}
	}
}
