package graphics;

import java.awt.Color;
import java.awt.Graphics;

import data.Direction;
import data.Pacman;

public class PacmanDrawer {
	private Pacman pacman;
	private int scale;
	private int drawingIteration;
	
	public PacmanDrawer(Pacman pacman, int scale) {
		this.pacman = pacman; // odavde vadimo informacije o pacmanu
		this.scale = scale; // velicina
		drawingIteration = 0;
	}

	public void draw(Graphics g) {
		g.setColor(Color.YELLOW); // bice zut, naravno
		
		int startAngle = 0; // pocetni ugao
		int arcAngle = 300; // zahvat
		
		if (!pacman.isDead()) { // ako je ziv animacija otvaranja i zatvaranja usta
			startAngle -= drawingIteration * 5;
			arcAngle += drawingIteration * 10;
		}
		
		if (pacman.getDirection() == Direction.LEFT) {
			startAngle += 210;
		} else if (pacman.getDirection() == Direction.RIGHT) {
			startAngle += 30;
		} else if (pacman.getDirection() == Direction.UP) {
			startAngle += 120;
		} else if (pacman.getDirection() == Direction.DOWN) { 
			startAngle += 300;
		} else {
			System.err.println("Unknown direction.");
		}
		
		g.fillArc((int)(pacman.getX() * scale), (int)(pacman.getY() * scale), scale, scale, startAngle, arcAngle);
		
		drawingIteration = (drawingIteration + 1) % 7; // sledeca iteracija
	}

}
