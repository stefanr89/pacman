package graphics;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import data.Direction;
import data.World;

public class PacmanListener implements KeyListener {
	private World world; // pacman objekat koji koristimo
	
	public PacmanListener(World world) {
		this.world = world;
	}

	@Override
	public void keyPressed(KeyEvent key) { // reaguje na strelice
		int code = key.getKeyCode();
		switch(code) {
		case KeyEvent.VK_UP:
			world.getPacman().setDirection(Direction.UP);
			break;
		case KeyEvent.VK_DOWN:
			world.getPacman().setDirection(Direction.DOWN);
			break;
		case KeyEvent.VK_LEFT:
			world.getPacman().setDirection(Direction.LEFT);
			break;
		case KeyEvent.VK_RIGHT:
			world.getPacman().setDirection(Direction.RIGHT);
			break;
		case KeyEvent.VK_SPACE:
			if (world.gameOver()) { // na space restartujemo igru, ako je pacman poginuo
				world.newGame();
			} else if (world.levelComplete()) {
				world.nextLevel();
			}
			break;
		case KeyEvent.VK_ESCAPE: 
			World.MORE_GAME = false;
			System.exit(0); // na escape izlazimo iz igre, bilo kad u toku rada programa
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
