package graphics;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import data.Ghost;
import data.World;

public class WorldView extends Canvas {
	private static final long serialVersionUID = 1L;
	private World world;
	private PacmanDrawer pd;
	private GhostDrawer[] gd;
	private final int scale = 16;
	private final int foodDiameter = scale/4;
	private final int powerupDiameter = scale/2;
	
	public WorldView(World world) {
		this.world = world;

		reload();
		
		addKeyListener(new PacmanListener(world));
		setSize(world.getWidth() * scale, world.getHeight() * scale);
	}
	
	public void reload() { // napravi nove drawere za pacmana i duhove
		this.pd = new PacmanDrawer(world.getPacman(), scale);
		
		Ghost[] ghosts = world.getGhosts();
		this.gd = new GhostDrawer[ghosts.length];
		for (int i=0; i<ghosts.length; i++) {
			gd[i] = new GhostDrawer(ghosts[i], scale);
		}
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.BLACK); // pozadina je crna
		g.fillRect(0, 0, getWidth(), getHeight()); // velicine celog prozora
		
		for (int x=0; x<world.getWidth(); x++) {
			for (int y=0; y<world.getHeight(); y++) {
				switch(world.getCell(x,y)) {
				case '.':  // zute tackice su hrana za pacmana
					g.setColor(Color.ORANGE);
					g.fillOval(x*scale + (scale - foodDiameter)/2, y*scale + (scale - foodDiameter)/2, foodDiameter, foodDiameter); 
					break;
				case 'o':  // ovo je powerup od koga duhovi poplave
					g.setColor(Color.ORANGE);
					g.fillOval(x*scale + (scale - powerupDiameter)/2, y*scale + (scale - powerupDiameter)/2, powerupDiameter, powerupDiameter); 
					break;
				case '#': // zidovi su pune plave linije
					g.setColor(Color.BLUE);
					g.fillRect(x*scale, y*scale, scale, scale);
					break;
				case '-': // zidovi su pune plave linije
					g.setColor(Color.PINK);
					g.fillRect(x*scale, y*scale, scale, scale);
					break;
				case ' ': // prazno polje
					break;
				default: // nepoznati simbol u mapi
					System.err.println("Unknown symbol: " + world.getCell(x,y));
				}
			}
		}
		
		pd.draw(g); // iscrtamo pacmana
		
		for (int i=0; i<gd.length; i++) {
			gd[i].draw(g);
		}
		
		if (world.gameOver() && world.getTime() % 80 > 40) {
			g.setColor(Color.WHITE);
			g.setFont(new Font("TimesRoman", Font.BOLD, 48)); 
			g.drawString("GAME OVER", 4*scale, 14*scale);
		}
	}
	
	public void update(Graphics g) {
		Image in = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
		paint(in.getGraphics());
		g.drawImage(in, 0, 0, null);
	}

}
