package graphics;

import java.awt.Color;
import java.awt.Graphics;

import data.Ghost;
import data.World;

public class GhostDrawer {
	private Ghost ghost;
	private int scale;

	public GhostDrawer(Ghost ghost, int scale) {
		this.ghost = ghost;
		this.scale = scale;
	}

	public void draw(Graphics g) {
		if (ghost.inFreightenMode()) {
			if (ghost.getWorld().getPowerupTimer() > World.POWERUP_DANGER) {
				g.setColor(new Color(60, 60, 205));
			} else {
				if (ghost.getWorld().getPowerupTimer() % 20 > 10)
					g.setColor(new Color(255, 255, 255));
				else {
					g.setColor(new Color(60, 60, 205));
				}
			}
		} else {
			g.setColor(ghost.getColor()); // boja koju inace ima
		}
		g.fillOval((int)(ghost.getX()*scale), (int)(ghost.getY()*scale), scale, scale);
		g.fillRect((int)(ghost.getX()*scale), (int)(ghost.getY()*scale) + scale/2, scale, scale/2);
		
		g.setColor(Color.WHITE); // oci
		g.fillOval((int)(ghost.getX()*scale) + scale/4, (int)(ghost.getY()*scale) + scale/2, scale/5, scale/5);
		g.fillOval((int)(ghost.getX()*scale) + scale/2, (int)(ghost.getY()*scale) + scale/2, scale/5, scale/5);
	}

}
