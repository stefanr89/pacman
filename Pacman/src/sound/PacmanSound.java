package sound;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class PacmanSound {
	private final String pacmanIntroSongFile = "intro.wav";
	private final String pacmanEatingFile =    "dot.wav";
	private final String pacmanPowerupFile =   "energizer.wav";
	private final String pacmanSirenFile =     "siren.wav";
	private final String pacmanDeathFile =     "death.wav";
	private final String pacmanGhostFile =     "ghost.wav";
	
	private boolean pacmanEating;
	
	private Clip intro;
	private Clip wakawaka;
	private Clip powerup;
	private Clip siren;
	private Clip death;
	private Clip ghost;
	
	public PacmanSound() {
		try {
			intro = AudioSystem.getClip(); ;
			intro.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanIntroSongFile)));
			
	        wakawaka = AudioSystem.getClip();
	        wakawaka.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanEatingFile)));
	        
	        powerup = AudioSystem.getClip();
	        powerup.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanPowerupFile)));
	        
	        siren = AudioSystem.getClip();
	        siren.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanSirenFile)));
	        
	        death = AudioSystem.getClip();
	        death.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanDeathFile)));
	        
	        ghost = AudioSystem.getClip();
	        ghost.open(AudioSystem.getAudioInputStream(getClass().getResource(pacmanGhostFile)));
	        
	        pacmanEating = false;
	        
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	public void playIntro() {
		intro.start();
	}
	
	public void stopIntro() {
		intro.stop();
		intro.setFramePosition(0);
	}
	
	public int durationIntro() {
		return (int)intro.getMicrosecondLength();
	}
	
	public void playPowerup() {
		powerup.loop(Clip.LOOP_CONTINUOUSLY);
	}
	
	public void stopPowerup() {
		powerup.stop();
	}
	
	public void playGhost() {
		ghost.start();
	}
	
	public void stopGhost() {
		ghost.stop();
		ghost.setFramePosition(0);
	}
	
	public void playWakaWaka() {
		if (!pacmanEating) {
			pacmanEating = true;
			wakawaka.loop(Clip.LOOP_CONTINUOUSLY);
		}
	}
	
	public int durationWakaWaka() {
		return (int)(wakawaka.getMicrosecondLength() / 1000);
	}
	
	public void stopWakaWaka() {
		if (pacmanEating) {
			pacmanEating = false;
			wakawaka.stop();
			wakawaka.setFramePosition(0);
		}
	}
	
	public void playSiren() {
		FloatControl gainControl = (FloatControl)siren.getControl(FloatControl.Type.MASTER_GAIN);
		gainControl.setValue(-10.0f); // Reduce volume by 10 decibels.
		siren.loop(Clip.LOOP_CONTINUOUSLY);
	}

	public void stopSiren() {
		siren.stop();
	}
	
	public void playDeath() {
		death.setMicrosecondPosition(1000000); // posto ima prazno vreme na pocetku
		death.start();
	}
	
	public void stopAllSounds() { // na kraju igre se poziva
		stopIntro();
		stopWakaWaka();
		stopSiren();
		stopPowerup();
		stopGhost();
	}
	
}
