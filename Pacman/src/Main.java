import graphics.PacmanListener;
import graphics.WorldView;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import sound.PacmanSound;
import data.World;


public class Main {
	public static final String TITLE = "Pac Man";
	public static final int TICK_TIME = 50; // 50 ms
	public static final int PREP_TIME = 4600; // 5 sec pre pocetka
	
	public static void main(String[] args) {
		Frame app = new Frame(TITLE); // naslov
		app.setResizable(false); // zabranjujemo resize
		app.addWindowListener(new WindowAdapter() { // na x gasimo program
		    @Override
		    public void windowClosing(WindowEvent windowEvent) {
		            System.exit(0);
		    }
		});
		
		PacmanSound sounds = new PacmanSound();
		
		World world = new World(sounds); // napravimo world
		WorldView board = new WorldView(world); // i prikaz za world
		app.add(board); // dodamo na jframe
		
		app.pack();
		app.setVisible(true);
		
		PacmanListener pacListener = new PacmanListener(world);
		app.addKeyListener(pacListener);
		
		while (World.MORE_GAME) {
			boolean preparation = true;
			board.reload();
			board.repaint();
			while (!world.gameOver()) { // dok ne stignemo do kraja igre
				try {
					if (preparation) {
						sounds.playIntro();
						Thread.sleep(PREP_TIME);
						preparation = false;
						sounds.playSiren();
					}
					world.tick(); // otkucamo 1 tick
					board.repaint(); // iscrtamo sve opet
					Thread.sleep(TICK_TIME); // cekamo sledeci tick
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			sounds.stopAllSounds();
			
			while(world.gameOver()) { // samo otkucavamo i iscrtavamo do restarta
				world.tick(); // otkucamo 1 tick
				board.repaint(); // iscrtamo sve opet
				try {
					Thread.sleep(TICK_TIME);  // cekamo sledeci tick
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
