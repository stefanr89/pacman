package data;

import java.awt.Color;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Ghost implements Runnable {
	private float ghx; // x koordinata
	private float ghy; // y koordinata
	private Color color; // boja duha
	private Direction currentDirection;
	private World world; // tabla u kojoj se duh nalazi
	
	private int lastX; // poslednji tile koji smo posetili, x koord
	private int lastY; // isto, y koord
	private int iteration; // u kojoj iteraciji smo
	private boolean started; // da li je thread za duha pokrenut
	
	private Mode currentMode; // duh se nalazi u hunt ili frighten modu
	private ModeHunt hm;
	private ModeFreighten fm;
	private int eatenTimeout; // ovoliko dugo, kada je duh pojeden, sedi u bazi
	
	private Semaphore mySem; // semafor na kome se blokira duh

	public Ghost(World world, float ghx, float ghy, Color color) {
		hm = new ModeHunt(); // hunt mod
		fm = new ModeFreighten(); // freighten mod
		eatenTimeout = 0;
		currentMode = hm; // pocnje se u hunt mode
		
		this.world = world;
		this.ghx = ghx;
		this.ghy = ghy;
		this.color = color;
		currentDirection = Direction.UP; // nekakav default
		
		mySem = new Semaphore(0); // 0 permits
		
		lastX = 0; // default vrednosti
		lastY = 0;
		iteration = 0;
		started = false;
	}
	
	public synchronized float getX() {
		return ghx;
	}
	
	public synchronized float getY() {
		return ghy;
	}
	
	public Color getColor() {
		return color;
	}
	
	public World getWorld() {
		return world;
	}
	
	/*
	 * Modovi ponasanja
	 */
	public synchronized void setHuntMode() {
		currentMode = hm;
	}
	
	public synchronized void setFrightenMode() {
		if (!world.inTheGhostHouse(Math.round(ghx), Math.round(ghy))) { // samo ako nije u kucici
			currentMode = fm;
		}
	}
	
	public synchronized boolean inHuntMode() {
		return currentMode == hm;
	}
	
	public synchronized boolean inFreightenMode() {
		return currentMode == fm;
	}

	/*
	 * U run se nalazi logika kretanja.
	 */
	@Override
	public void run() {
		started = true;
		while (!world.gameOver()) {
			if (eatenTimeout-- > 0) { // kada pojedemo duha, on neko vreme ne radi nista u bazi
				block(); // cekamo sledeci potez
				continue; // i ne radimo nista vise u ovoj iteraciji
			}
			
			int cellX;
			int cellY;
			int pacX;
			int pacY;
			float delta;
			Direction currentDir;
			
			synchronized(this) {
				cellX = Math.round(ghx);
				cellY = Math.round(ghy);
				currentDir = currentDirection;
				pacX = Math.round(world.getPacman().getX());
				pacY = Math.round(world.getPacman().getY());
				delta = currentMode.getDelta(); // delta se menja u zavisnosti od moda
				
				if (cellX == pacX && cellY == pacY) { // ako se poklapaju koordinate duha sa pacmanovim
					if (inFreightenMode()) { // ako je pacman pod powerupom vracamo duha u kucicu
						ghx = 13.5f;
						ghy = 14.0f;
						currentMode = hm; // vratimo ga i u hunt mode
						eatenTimeout = World.GHOST_EATEN_TIMEOUT; // i postavimo timeout koji ce provesti u bazi
						world.killGhost();
					} else {
						world.killPacman();
					}
					continue; // nema sta da radimo vise u ovoj u iteraciji
				}
			}
			
			float nextGhx = ghx;
			float nextGhy = ghy;
			
			if ((cellX != lastX || cellY != lastY) && Math.abs(Math.round(ghx) - ghx) < delta/2 && Math.abs(Math.round(ghy) - ghy) < delta/2) { // ako vec nismo posetili ovaj tile
				lastX = cellX; // oznacimo ga kao posecenog
				lastY = cellY;
				
				nextGhx = cellX; // normalizujemo x i y
				nextGhy = cellY;
				List<Direction> possibleDirections = world.possibleDirections(cellX, cellY); // dohvatimo moguce pravce
				
				synchronized(this) {
					possibleDirections.remove(currentDir.getOppositeDirection()); // izbacimo suprotni
				}
				
				if (world.inTheGhostHouse(cellX, cellY)) { // ako je u kucici, mora prvo da izadje
					int targetX = 13;
					int targetY = 11;
					currentDir = currentMode.bestDirection(cellX, cellY, targetX, targetY, possibleDirections); // najbolji pravac do izlaza
				} else if (iteration > 0) { // 3 puta targetuje, cetvrti put bira na random
					currentDir = currentMode.bestDirection(cellX, cellY, pacX, pacY, possibleDirections); // najbolji pravac biramo za sledeci
				} else {
					currentDir = possibleDirections.get((int)(Math.random() * possibleDirections.size())); // izaberemo neki pravac nasumicno
				}
				iteration = (iteration + 1) % 4;
			}
			
			if (currentDir == Direction.UP) {
				nextGhy -= delta;
			} else if (currentDir == Direction.DOWN) {
				nextGhy += delta;
			} else if (currentDir == Direction.LEFT) {
				nextGhx -= delta;
				if(nextGhx < -0.5f && Math.abs(ghy - world.getTunnelRow()) < delta/2) { // duhovi ne prolaze kroz tunel nego se okrenu
					currentDir = currentDir.getOppositeDirection();
				}
			} else if (currentDir == Direction.RIGHT) {
				nextGhx += delta;
				if(nextGhx >= world.getWidth()-0.5f && Math.abs(ghy - world.getTunnelRow()) < delta/2) { // isto kao gore
					currentDir = currentDir.getOppositeDirection();
				}
			}
			
			synchronized(this) {
				if (world.canMove(cellX, cellY, currentDir)) { // ako moze da ide dalje, neka ide
					ghx = nextGhx;
					ghy = nextGhy;
				} else { // ako ne moze, rounduje se na poslednju legalnu vrednost
					ghx = Math.round(ghx);
					ghy = Math.round(ghy);
				}
				currentDirection = currentDir;
			}
			
			block(); // na kraju poteza se blokiramo i cekamo sledeci
		}
	}
	
	private void block() {
		try {
			mySem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void tick() {
		if (started) {
			mySem.release();
			
			if (world.getPowerupTimer() == 0) {
				setHuntMode();
			}
		}
	}

}
