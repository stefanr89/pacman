package data;

import java.util.List;

public class ModeHunt implements Mode {
	private static final float DEFAULT_DELTA = 0.16f;
	
	private float delta;
	
	public ModeHunt() {
		delta = DEFAULT_DELTA;
	}

	/*
	 * Ovde trazimo sto kracu distancu, jer jurimo pacmana.
	 */
	@Override
	public Direction bestDirection(int cellX, int cellY, int targetX, int tagetY, List<Direction> possibleDirections) {
		double bestDistance = Double.POSITIVE_INFINITY;
		Direction bestDirection = null;
		
		for (Direction d: possibleDirections) { // prodjemo kroz sve moguce pravce
			int myX = cellX;
			int myY = cellY;
			if (d == Direction.UP) { // azuriramo ih
				myY -= 1;
			} else if (d == Direction.DOWN) {
				myY += 1;
			} else if (d == Direction.LEFT) {
				myX -= 1;
			} else if (d == Direction.RIGHT) {
				myX += 1;
			}
			
			double distance = Math.sqrt(Math.pow(targetX-myX, 2) + Math.pow(tagetY-myY, 2)); // nadjemo rastojanje
			if (distance < bestDistance) { // ako je krace od najboljeg azuriramo
				bestDistance = distance;
				bestDirection = d;
			}
		}
		
		return bestDirection;
	}

	@Override
	public float getDelta() {
		return delta;
	}

}
