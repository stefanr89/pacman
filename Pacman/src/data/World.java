package data;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sound.PacmanSound;

public class World { // walls 28x31
	private final char[][] map =   {  { '#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' }, // 1
									  { '#','.','.','.','.','.','.','.','.','.','.','.','.','#','#','.','.','.','.','.','.','.','.','.','.','.','.','#' }, // 2
									  { '#','.','#','#','#','#','.','#','#','#','#','#','.','#','#','.','#','#','#','#','#','.','#','#','#','#','.','#' }, // 3
									  { '#','o','#','#','#','#','.','#','#','#','#','#','.','#','#','.','#','#','#','#','#','.','#','#','#','#','o','#' }, // 4
									  { '#','.','#','#','#','#','.','#','#','#','#','#','.','#','#','.','#','#','#','#','#','.','#','#','#','#','.','#' }, // 5
									  { '#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#' }, // 6
									  { '#','.','#','#','#','#','.','#','#','.','#','#','#','#','#','#','#','#','.','#','#','.','#','#','#','#','.','#' }, // 7
									  { '#','.','#','#','#','#','.','#','#','.','#','#','#','#','#','#','#','#','.','#','#','.','#','#','#','#','.','#' }, // 8
									  { '#','.','.','.','.','.','.','#','#','.','.','.','.','#','#','.','.','.','.','#','#','.','.','.','.','.','.','#' }, // 9
									  { '#','#','#','#','#','#','.','#','#','#','#','#',' ','#','#',' ','#','#','#','#','#','.','#','#','#','#','#','#' }, // 10
									  { ' ',' ',' ',' ',' ','#','.','#','#','#','#','#',' ','#','#',' ','#','#','#','#','#','.','#',' ',' ',' ',' ',' ' }, // 11
									  { ' ',' ',' ',' ',' ','#','.','#','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#','#','.','#',' ',' ',' ',' ',' ' }, // 12
									  { ' ',' ',' ',' ',' ','#','.','#','#',' ','#','#','#','-','-','#','#','#',' ','#','#','.','#',' ',' ',' ',' ',' ' }, // 13
									  { '#','#','#','#','#','#','.','#','#',' ','#',' ',' ',' ',' ',' ',' ','#',' ','#','#','.','#','#','#','#','#','#' }, // 14
									  { ' ',' ',' ',' ',' ',' ','.',' ',' ',' ','#',' ',' ',' ',' ',' ',' ','#',' ',' ',' ','.',' ',' ',' ',' ',' ',' ' }, // 15
									  { '#','#','#','#','#','#','.','#','#',' ','#',' ',' ',' ',' ',' ',' ','#',' ','#','#','.','#','#','#','#','#','#' }, // 16
									  { ' ',' ',' ',' ',' ','#','.','#','#',' ','#','#','#','#','#','#','#','#',' ','#','#','.','#',' ',' ',' ',' ',' ' }, // 17
									  { ' ',' ',' ',' ',' ','#','.','#','#',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','#','#','.','#',' ',' ',' ',' ',' ' }, // 18
									  { ' ',' ',' ',' ',' ','#','.','#','#',' ','#','#','#','#','#','#','#','#',' ','#','#','.','#',' ',' ',' ',' ',' ' }, // 19
									  { '#','#','#','#','#','#','.','#','#',' ','#','#','#','#','#','#','#','#',' ','#','#','.','#','#','#','#','#','#' }, // 20
									  { '#','.','.','.','.','.','.','.','.','.','.','.','.','#','#','.','.','.','.','.','.','.','.','.','.','.','.','#' }, // 21
									  { '#','.','#','#','#','#','.','#','#','#','#','#','.','#','#','.','#','#','#','#','#','.','#','#','#','#','.','#' }, // 22
									  { '#','.','#','#','#','#','.','#','#','#','#','#','.','#','#','.','#','#','#','#','#','.','#','#','#','#','.','#' }, // 23
									  { '#','o','.','.','#','#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#','#','.','.','o','#' }, // 24
									  { '#','#','#','.','#','#','.','#','#','.','#','#','#','#','#','#','#','#','.','#','#','.','#','#','.','#','#','#' }, // 25
									  { '#','#','#','.','#','#','.','#','#','.','#','#','#','#','#','#','#','#','.','#','#','.','#','#','.','#','#','#' }, // 26
									  { '#','.','.','.','.','.','.','#','#','.','.','.','.','#','#','.','.','.','.','#','#','.','.','.','.','.','.','#' }, // 27
									  { '#','.','#','#','#','#','#','#','#','#','#','#','.','#','#','.','#','#','#','#','#','#','#','#','#','#','.','#' }, // 28
									  { '#','.','#','#','#','#','#','#','#','#','#','#','.','#','#','.','#','#','#','#','#','#','#','#','#','#','.','#' }, // 29
									  { '#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#' }, // 30
									  { '#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' }, // 31
	};
	
	private char[][] walls;
	public static final int DOT_BONUS = 10; // bonus koji se dobije kada se pojede tacka
	public static final int POWERUP_BONUS = 50; // bonus za pojeden powerup
	public static final int GHOST_KILL = 200; // bonus za pojedenog duha
	public static final int GHOST_BONUS_KILL = 400; // bonus za pojedenog duha dok blinka
	
	public static final int POWERUP_DURATION = 250; // koliko taktova traje powerup
	public static final int POWERUP_DANGER = 100; // na koliko taktova duhovi pocinju da blinkaju
	public static final int GHOST_EATEN_TIMEOUT = 200; // koliko taktova timeout za pojedenog duha
	
	public static boolean MORE_GAME = true;
	
	private final int tunnelRow = 14;
	
	private int time; // broj tickova koji su prosli
	private int score; // ukupan rezultat
	private boolean gameEnd; // da li je kraj igre
	
	private Pacman pacman; // pacman
	private Ghost[] ghosts; // duhovi
	
	private PacmanSound sounds;
	private int playFor;
	
	private int powerupTimer;
	private int dotsRemaining; // preostali broj tackica na ovom nivou
	
	public World(PacmanSound sounds) {
		this.sounds = sounds;

		restart(); // iskoristicemo postojecu funkciju
		
		score = 0;
		playFor = 0;
		
		time = 0; // i brojac vremena ide na 0
		powerupTimer = 0; // nismo pod powerupom na pocetku
		
		gameEnd = false;
	}
	
	public void restartTime() { // vreme restartujemo samo ako pacman pogine, ne i ako se prelazi na sledeci nivo
		time = 0; // zato je izdvojeno iz funkcije za restart
		score = 0; // i score
	}
	
	public void restart() { // restartuje svet
		walls = new char[map.length][map[0].length]; // duboka kopija mape
		for (int i = 0; i < walls.length; i++)
			walls[i] = Arrays.copyOf(map[i], map[i].length);
		
		dotsRemaining=0;
		for (int i=0; i<getWidth(); i++)
			for (int j=0; j<getHeight(); j++)
				if (isEatable(i, j)) {
					dotsRemaining++;
				}
		
		pacman = new Pacman(this, 14, 23); // restartuje se i pacman
		
		ghosts = new Ghost[4]; // i duhovi
		ghosts[0] = new Ghost(this, 13.5f, 11.0f, Color.RED);
		ghosts[1] = new Ghost(this, 13.5f, 14.0f, Color.PINK);
		ghosts[2] = new Ghost(this, 11.0f, 14.0f, Color.CYAN);
		ghosts[3] = new Ghost(this, 16.0f, 14.0f, Color.ORANGE);
	}
	
	public Pacman getPacman() { // vraca objekat pacmana
		return pacman;
	}
	
	public Ghost[] getGhosts() { // vraca niz sa objektima duhova
		return ghosts;
	}
	
	public int getWidth() { // sirina tabele
		return map[0].length;
	}
	
	public int getHeight() { // visina tabele
		return map.length;
	}
	
	

	public char getCell(int x, int y) { // sadrzaj celije
		return walls[y][x];
	}
	
	public void eatCell(int x, int y) { // pacman jede celiju
		if (walls[y][x] == '.') {
			sounds.playWakaWaka();
			playFor = 10;
			score += DOT_BONUS;
			dotsRemaining--;
		} else if (walls[y][x] == 'o') {
			sounds.playPowerup();
			powerupTimer = POWERUP_DURATION;
			for (Ghost g: ghosts) {
				g.setFrightenMode();
			}
			score += POWERUP_BONUS;
			dotsRemaining--;
		}
		if (levelComplete()) { // ako smo pojeli sve, kraj igre
			gameEnd = true;
		}
		walls[y][x] = ' '; // polje postaje prazno
	}
	
	public boolean isWall(int x, int y) { // da li je polje na x-y zid
		if (x < 0 || x >= getWidth() || y < 0 || y >= getHeight()) { // ovo moze da se desi kod tunela, koji tehnicki nije zid
			return false;
		}
		return walls[y][x] == '#';
	}
	
	public boolean isGhostWall(int x, int y) { // da li je polje na x-y izlaz na kucici za duhove
		if (x < 0 || x >= getWidth() || y < 0 || y >= getHeight()) { // ovo moze da se desi kod tunela, koji tehnicki nije zid
			return false;
		}
		return walls[y][x] == '-';
	}
	
	public boolean isEatable(int x, int y) {
		if (x < 0 || x >= getWidth() || y < 0 || y >= getHeight()) { // ovo moze da se desi kod tunela, koji tehnicki nije zid
			return false;
		}
		return walls[y][x] == '.' || walls[y][x] == 'o';
	}
	
	public boolean inTheGhostHouse(int x, int y) {
		return x > 10 && x <17 && y > 12 && y <17; // proverava da li smo u kucici za duhove
	}
	
	/*
	 * Svi pravci koji se mogu uzeti
	 */
	public List<Direction> possibleDirections(int x, int y) {
		List<Direction> dirs = new ArrayList<Direction>();
		boolean inGhostHouse = inTheGhostHouse(x,y); // ako nismo u kuci, moramo da pazimo da ne prodjemo kroz ghost wall
		if (!isWall(x-1, y) && (inGhostHouse || !isGhostWall(x-1, y))) {
			dirs.add(Direction.LEFT);
		}
		if (!isWall(x+1, y) && (inGhostHouse || !isGhostWall(x+1, y))) {
			dirs.add(Direction.RIGHT);
		}
		if (!isWall(x, y-1) && (inGhostHouse || !isGhostWall(x, y-1))) {
			dirs.add(Direction.UP);
		}
		if (!isWall(x, y+1) && (inGhostHouse || !isGhostWall(x, y+1))) {
			dirs.add(Direction.DOWN);
		}
		return dirs;
	}
	
	/*
	 * Da li smo na raskrsnici
	 */
	public boolean isIntersection(int x, int y) {
		return possibleDirections(x, y).size() >= 3;
	}
	
	/*
	 * Da li se sa pozicije x-y moze ici u smeru dir
	 */
	public boolean canMove(int x, int y, Direction dir) {
		int nextX = x;
		int nextY = y;
		
		if (dir == Direction.UP) {
			nextY -= 1;
		} else if (dir == Direction.DOWN) {
			nextY += 1;
		} else if (dir == Direction.LEFT) {
			nextX -= 1;
		} else if (dir == Direction.RIGHT) {
			nextX += 1;
		}
		
		if (nextX < 0 || nextX >= getWidth()) { // ne mozemo da se krecemo van table
			if (nextY == tunnelRow) { // ako se krece kroz tunel moze da prodje
				return true;
			} else { // u svim ostalim slucajevima ne moze
				return false; 
			}
		}
		
		if (nextY < 0 || nextY >= getHeight()) { // ne mozemo da se krecemo van table
			return false;
		}
		
		if (inTheGhostHouse(x, y)) {
			return !isWall(nextX, nextY); // ako smo u kucici mozemo da prodjemo kroz ghost wall, ali ne kroz obican zid
		} else {
			return !(isWall(nextX, nextY) || isGhostWall(nextX, nextY)); // cim izadjemo iz kucice ne mozemo da se vratimo
		}
	}
	
	
	public void tick() { // otkucava 1 tick
		if (!gameOver()) { // obavestava ostale objekte dokle god je igra u toku
			pacman.tick(); // obavesti i pacmana
			if (time == 0) { // startuj duhove, ako vec nisi
				new Thread(ghosts[0]).start();
				new Thread(ghosts[1]).start();
			} else if (time == 60) {
				new Thread(ghosts[2]).start();
			} else if (time == 200) {
				new Thread(ghosts[3]).start();
			}
			
			for (int i=0; i<ghosts.length; i++) { // tick i duhovima
				ghosts[i].tick();
			}
			
			if (playFor > 0) { // zvuci koje pacman pravi kad jede
				playFor--;
			} else {
				sounds.stopWakaWaka();
			}
		}
		
		synchronized(this) {
			time++; // azuriramo vreme
			if (powerupTimer > 0) { // i powerup timer, ako je > 0
				powerupTimer--;
				if (powerupTimer == 0) sounds.stopPowerup();
			}
		}
	}
	
	public synchronized int getTime() { // vraca trenutno vreme
		return time;
	}
	
	public synchronized int getScore() { // vraca score
		return score;
	}
	
	public synchronized void killPacman() { // duh je dodirnuo pacmana
		sounds.playDeath();
		pacman.setDead();
		gameEnd = true;
	}
	
	public synchronized void killGhost() {
		sounds.stopGhost(); // rewindujemo clip
		sounds.playGhost(); // pa ga pustimo
		if (powerupTimer > POWERUP_DANGER) { // poeni za ubijenog duha
			score += GHOST_KILL;
		} else { // vise poena se dobije za kill kada duh blinka
			score += GHOST_BONUS_KILL;
		}
	}

	public boolean gameOver() { // da li je kraj igre
		return gameEnd;
	}

	public int getTunnelRow() { // y koordinata reda u kome se nalazi tunel
		return tunnelRow;
	}

	public int getPowerupTimer() {
		return powerupTimer;
	}

	public void newGame() { // zapocinje novu igru
		gameEnd = false;
		restart(); // restartujemo tabelu, pacmana i duhove
		restartTime(); // posto je pacman poginuo, krecemo ispocetka, restartuju se i vreme, nivo i score
	}

	public boolean levelComplete() { // da li smo zavrsili nivo
		return dotsRemaining == 0;
	}

	public void nextLevel() {
		gameEnd = false;
		restart(); // restartujemo polozaje, ali ostavljamo nivo, score i vreme
	}
}
