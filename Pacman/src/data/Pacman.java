package data;

public class Pacman {
	private Direction currentDirection; // pravac u kome se krece pacman
	private Direction nextDirection; // sledeci pravac u kome ce se pacman kretati, kada bude u mogucnosti
	private World world; // tabla u kojoj se pacman nalazi
	private float pacx; // x coord
	private float pacy; // y coord
	private float delta; // brzina kojom se pacman krece
	private boolean dead; // da li je pacman poginuo
	
	private static final float DEFAULT_DELTA = 0.2f;
	
	public Pacman(World world, float pacx, float pacy, float delta) {
		currentDirection = Direction.LEFT; // na pocetku je okrenut u levo
		nextDirection = Direction.LEFT;
		this.world = world;
		this.pacx = pacx;
		this.pacy = pacy;
		this.delta = delta; // ne pomera se za celo polje nego samo delimicno
		this.dead = false;
	}
	
	public Pacman(World world, float pacx, float pacy) { // sa default vrednoscu za brzinu
		this(world, pacx, pacy, DEFAULT_DELTA);
	}
	
	/*
	 * Menja pravac kretanja pacmana
	 */
	public void setDirection(Direction newDir) {
		nextDirection = newDir;
	}
	
	public void tick() { // vreme otkucava i na svaki otkucaj pacman se pomera
		if (currentDirection.getOppositeDirection() == nextDirection) {
			currentDirection = nextDirection;
		} else if (Math.abs(Math.round(pacx) - pacx) < delta/2 && Math.abs(Math.round(pacy) - pacy) < delta/2 // da se ne sece sa zidovima
				&& world.canMove(Math.round(pacx), Math.round(pacy), nextDirection)){ // sa ovim uslovom se krece u prethodnom smeru dokle god ne moze da skrene
			currentDirection = nextDirection;
		}
		
		float nextPacx = pacx;
		float nextPacy = pacy;
		
		if (currentDirection == Direction.UP) {
			nextPacy -= delta;
		} else if (currentDirection == Direction.DOWN) {
			nextPacy += delta;
		} else if (currentDirection == Direction.LEFT) {
			nextPacx -= delta;
			if(nextPacx < -0.5f && Math.abs(pacy - world.getTunnelRow()) < delta/2) { // ako prolazimo kroz tunel na sredini
				nextPacx = world.getWidth()-1;
			}
		} else if (currentDirection == Direction.RIGHT) {
			nextPacx += delta;
			if(nextPacx >= world.getWidth()-0.5f && Math.abs(pacy - world.getTunnelRow()) < delta/2) { // ako prolazimo kroz tunel na sredini
				nextPacx = 0;
			}
		}
		
		int cellX = Math.round(pacx);
		int cellY = Math.round(pacy);
		world.eatCell(cellX, cellY); // pojede tacku sa trenutne pozicije
		
		if (world.canMove(cellX, cellY, currentDirection)) { // ako moze da ide dalje, neka ide
			pacx = nextPacx;
			pacy = nextPacy;
		} else { // ako ne moze, rounduje se na poslednju legalnu vrednost
			pacx = Math.round(pacx);
			pacy = Math.round(pacy);
		}
		
	}

	// geteri
	public synchronized float getX() {
		return pacx;
	}

	public synchronized float getY() {
		return pacy;
	}

	public synchronized Direction getDirection() {
		return currentDirection;
	}

	public synchronized World getWorld() {
		return world;	
	}
	
	public synchronized boolean isDead() {
		return dead;
	}

	public synchronized void setDead() {
		dead = true;
	}
}
