package data;

public enum Direction{
	UP, 
	DOWN, 
	LEFT, 
	RIGHT;
	
	private Direction opposite; // nije lose da imamo i suprotni pravac od trenutnog
	
	static { // to sve ovde prevezemo
		UP.opposite = DOWN;
		DOWN.opposite = UP;
		LEFT.opposite = RIGHT;
		RIGHT.opposite = LEFT;
	}
	
	public Direction getOppositeDirection() { // i metodu za dohvatanje
        return opposite;
    }
}
