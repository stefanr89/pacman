package data;

import java.util.List;

public interface Mode {
	Direction bestDirection(int cellX, int cellY, int targetX, int tagetY, List<Direction> possibleDirections);
	float getDelta();
}
